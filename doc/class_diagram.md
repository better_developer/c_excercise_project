~~~mermaid
classDiagram
    UserInfoList <|-- ClientList
    UserInfoList <|-- GatewayList

    ClientList <|-- Heartbeat
    GatewayList <|-- Heartbeat

    ClientList <|-- UdpPacketServer
    GatewayList <|-- UdpPacketServer

    ClientList <|-- MsgForward
    GatewayList <|-- MsgForward

    Heartbeat <|-- ListenMsg
    UdpPacketServer <|-- ListenMsg
    MsgForward <|-- ListenMsg

    class UserInfoList {
        struct user_info_list
        UserInfoList_Init()
        UserInfoList_Insert()
    }

    class ClientList {
        search()
        insert()
        remove()
        print()
        update()
    }

    class GatewayList {
        search()
        insert()
        remove()
        print()
        update()
    }

    class Heartbeat {
        checkClientThread()
        checkGatewayThread()
        handleClientHeartbeat()
        handleGatewayHeartbeat()
    }

    class UdpPacketServer {
        handleClientConnect()
        handleGatewayConnect()
        handleClientConnectNotice()
        sendConnectResponse()
        handleClientRelease()
        handleGatewayRelease()
        handleClientReleaseNotice()
    }

    class MsgForward {
        forwarChatMsg()
        sendForwarResponse()
    }
~~~

