~~~mermaid
sequenceDiagram
    participant Source
    participant Listener
    participant MsgProcessFunc
    participant ClientList
    participant GatewayList
    participant Target

    Note over Source: Connected with Server already

	Source->>Listener: 1 send chat message
    Listener->>MsgProcessFunc: 2 MsgProcessFunc_ForwardChatMsg(recv_buff, peer_addr)
    note over MsgProcessFunc: get target_client_id from recv_buff
    MsgProcessFunc->>ClientList: 3 getForwardingAddr(target_client_id)
    note over ClientList: get index by ClientList_Search(target_client_id)
    note over ClientList: get connected gateway_id by ClientList_GetGatewayId(index)
    alt gateway_id == 0
    	ClientList->>MsgProcessFunc: 4 return ClientList_GetClientAddr(target_client_id)
    else gateway_id != 0
    	ClientList->>GatewayList: 5 GatewayList_GetGatewayAddr(gateway_id)
    	GatewayList->>MsgProcessFunc: 5a return connected-gateway address
    end
    MsgProcessFunc->>Target: 6 forward_status = sendto(), forward chat message to client or client-connected gateway
    alt forward successfully
        MsgProcessFunc->>Source: 7 sendForwardingResponse(forward_status(>0), recv_buff, peer_addr)
    else Falied
        MsgProcessFunc->>Source: 8 sendForwardingResponse(forward_status(<=0), recv_buff, peer_addr)
    end
~~~

