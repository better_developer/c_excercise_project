~~~mermaid
sequenceDiagram
    participant Server

    note over Server: signal(SIGINT, deinitServer)
    note over Server: ClientList_Init() <br> GatewayList_Init() <br> ServerSocket_Init();
    note over Server: thread 1: Heartbeat_UpdateUserStatus() <br> thread 2: Listener_ProcessMsg()
~~~

