~~~mermaid
sequenceDiagram
    participant Client
    participant Listener
    participant MsgProcessFunc
    participant ClientList

    Client->>Listener: 1 send client connection notice
    Listener->>MsgProcessFunc: 2 MsgProcessFunc_ProcessClientConnectionRequest(recv_buff, client_addr)
    note over MsgProcessFunc: get client_id from recv_buff
    
    MsgProcessFunc->>ClientList: 3 ClientList_IsFull()
    ClientList->>MsgProcessFunc: 4 return outcome
    alt USER_LIST_FULL
        note over MsgProcessFunc: processFullClientListCase(recv_buff, client_addr)
        MsgProcessFunc->>Client: sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, client_addr) <br> with err_code = CONNECTED_ERR_OF_LIST_FULL
        note over MsgProcessFunc: return
    end

    MsgProcessFunc->>ClientList: 5 ClientList_Exist(client_id)
    ClientList->>MsgProcessFunc: 6 return outcome
    alt USER_LIST_SEARCH_SUCCESS
        note over MsgProcessFunc: processClientExistCase(client_id, recv_buff, client_addr)
        MsgProcessFunc->>Client: sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, client_addr) <br> with err_code = CONNECTED_ERR_OF_ID_EXISTED
        note over MsgProcessFunc: return
    end

    note over MsgProcessFunc: processSuccessfulClientConn(client_id, 0, recv_buff, client_addr)
    MsgProcessFunc->>ClientList: 7 ClientList_Add(client_id, gateway_id, client_addr)
    ClientList->>MsgProcessFunc: 8 return add_flag
    alt USER_LIST_ADD_SUCCESS
        MsgProcessFunc->>Client: sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, client_addr) <br> with err_code = 0
    else USER_LIST_ADD_FAIL
        MsgProcessFunc->>Client: sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, client_addr) <br> with err_code = CONNECTED_ERR_OF_ADD_UNSUCCESSFULLY
    end
~~~

~~~mermaid
sequenceDiagram
    participant Gateway
    participant Listener
    participant MsgProcessFunc
    participant ClientList

    Gateway->>Listener: 1 send client connection request
    Listener->>MsgProcessFunc: 2 MsgProcessFunc_ProcessClientConnectionNotice(recv_buff, gateway_addr)
    note over MsgProcessFunc: get client_id from recv_buff
    
    MsgProcessFunc->>ClientList: 3 ClientList_IsFull()
    ClientList->>MsgProcessFunc: 4 return outcome
    alt USER_LIST_FULL
        note over MsgProcessFunc: processFullClientListCase(recv_buff, gateway_addr)
        MsgProcessFunc->>Gateway: sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr) <br> with err_code = CONNECTED_ERR_OF_LIST_FULL
        note over MsgProcessFunc: return
    end

    MsgProcessFunc->>ClientList: 5 ClientList_Exist(client_id)
    ClientList->>MsgProcessFunc: 6 return outcome
    alt USER_LIST_SEARCH_SUCCESS
        note over MsgProcessFunc: processClientExistCase(client_id, recv_buff, gateway_addr)
        MsgProcessFunc->>Gateway: sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr) <br> with err_code = CONNECTED_ERR_OF_ID_EXISTED
        note over MsgProcessFunc: return
    end

    note over MsgProcessFunc: processSuccessfulClientConn(client_id, gateway, recv_buff, gateway_addr)
    MsgProcessFunc->>ClientList: 7 ClientList_Add(client_id, gateway_id, null_addr)
    ClientList->>MsgProcessFunc: 8 return add_flag
    alt USER_LIST_ADD_SUCCESS
        MsgProcessFunc->>Gateway: sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr) <br> with err_code = 0
    else USER_LIST_ADD_FAIL
        MsgProcessFunc->>Gateway: sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr) <br> with err_code = CONNECTED_ERR_OF_ADD_UNSUCCESSFULLY
    end
~~~

~~~mermaid
sequenceDiagram
    participant Gateway
    participant Listener
    participant MsgProcessFunc
    participant GatewayList

    Gateway->>Listener: 1 send gateway connection request
    Listener->>MsgProcessFunc: 2 MsgProcessFunc_ProcessGatewayConnectionRequest(recv_buff, gateway_addr)
    note over MsgProcessFunc: get gateway_id from recv_buff
    
    MsgProcessFunc->>GatewayList: 3 GatewayList_IsFull()
    GatewayList->>MsgProcessFunc: 4 return outcome
    alt USER_LIST_FULL
        note over MsgProcessFunc: processFullGatewayListCase(recv_buff, gateway_addr)
        MsgProcessFunc->>Gateway: sendConnectionResponse(GATEWAY_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr) <br> with err_code = CONNECTED_ERR_OF_LIST_FULL
        note over MsgProcessFunc: return
    end

    MsgProcessFunc->>GatewayList: 5 GatewayList_Exist(gateway_id)
    GatewayList->>MsgProcessFunc: 6 return outcome
    alt USER_LIST_SEARCH_SUCCESS
        note over MsgProcessFunc: processGatewayExistCase(gateway_id, recv_buff, gateway_addr)
        MsgProcessFunc->>Gateway: sendConnectionResponse(GATEWAY_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr) <br> with err_code = CONNECTED_ERR_OF_ID_EXISTED
        note over MsgProcessFunc: return
    end

    note over MsgProcessFunc: processSuccessfulGatewayConn(gateway_id, recv_buff, gateway_addr)
    MsgProcessFunc->>GatewayList: 7 GatewayList_Add(gateway_id, gateway_addr)
    GatewayList->>MsgProcessFunc: 8 return add_flag
    alt USER_LIST_ADD_SUCCESS
        MsgProcessFunc->>Gateway: sendConnectionResponse(GATEWAY_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr) <br> with err_code = 0
    else USER_LIST_ADD_FAIL
        MsgProcessFunc->>Gateway: sendConnectionResponse(GATEWAY_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr) <br> with err_code = CONNECTED_ERR_OF_ADD_UNSUCCESSFULLY
    end
~~~

