~~~mermaid
sequenceDiagram
    participant Client
    participant Listener
    participant MsgProcessFunc
    participant ClientList

    Client->>Listener: 1 send client release request
    Listener->>MsgProcessFunc: 2 MsgProcessFunc_ProcessClientReleaseRequest(recv_buff)
    note over MsgProcessFunc: get client_id from recv_buff
    MsgProcessFunc->>ClientList: 3 ClientList_Remove(client_id)
~~~

~~~mermaid
sequenceDiagram
    participant Gateway
    participant Listener
    participant MsgProcessFunc
    participant GatewayList
    participant ClientList

    Gateway->>Listener: 1 send gateway release request
    Listener->>MsgProcessFunc: 2 MsgProcessFunc_ProcessGatewayReleaseRequest(recv_buff)
    note over MsgProcessFunc: get gateway_id from recv_buff
    MsgProcessFunc->>GatewayList: 3 GatewayList_Remove(gateway_id)
    MsgProcessFunc->>ClientList: 4 ClientList_RemoveClientsConnectedOfflineGateway(gateway_id)
    loop
        note over ClientList: get client_id when client connects to this gateway
        note over ClientList: ClientList_Remove(client_id)
    end
~~~

~~~mermaid
sequenceDiagram
    participant Gateway
    participant Listener
    participant MsgProcessFunc
    participant GatewayList
    participant ClientList

    Gateway->>Listener: 1 send client release notice
    Listener->>MsgProcessFunc: 2 MsgProcessFunc_ProcessClientReleaseNotice(recv_buff)
    note over MsgProcessFunc: get client_id from recv_buff
    MsgProcessFunc->>ClientList: 3 ClientList_Remove(client_id)
~~~

