~~~mermaid
sequenceDiagram
    participant User
    participant Listener
    participant MsgProcessFunc
    participant ClientList
    participant GatewayList

    User->>Listener: 1 send heartbeat packet
    alt client_heartbeat
        Listener->>MsgProcessFunc: 2 MsgProcessFunc_ProcessClientHeartbeat(recv_buff)
        note over MsgProcessFunc: get client_id from recv_buff
        MsgProcessFunc->>ClientList: 2a Client_Exist(client_id)
        alt return USER_LIST_SEARCH_SUCCESS
        	note over ClientList: print "The client doesn't exist." and return
        else return USER_LIST_SEARCH_FAIL
        	note over ClientList: Client_ResetTime(client_id)
        end
    else gateway_heartbeat
        Listener->>MsgProcessFunc: 3 MsgProcessFunc_ProcessGatewayHeartbeat(recv_buff)
        note over MsgProcessFunc: get gateway_id from recv_buff
        MsgProcessFunc->>ClientList: 3a GatewayList_Exist(gateway_id)
        alt return USER_LIST_SEARCH_SUCCESS
        	note over GatewayList: print "The gateway doesn't exist." and return
        else return USER_LIST_SEARCH_FAIL
        	note over GatewayList: GatewayList_ResetTime(gateway_id)
        end
    end

~~~

~~~mermaid
sequenceDiagram
    participant Heartbeat
    participant ClientList
    participant GatewayList
    
    note over Heartbeat: reset print_timestamp
    loop
    	Heartbeat->>ClientList: 1 ClientList_Update()
    	loop
    		note over ClientList: get client_timestamp
    		alt client_timestamp > kUSER_LONGETH_SURVIVAL_TIME
    			note over ClientList:  ClientList_Remove(client_id)
    		end
    	end
    	Heartbeat->>GatewayList: 2 GatewayList_Update()
    	loop
    		note over GatewayList: get gateway_timestamp
    		alt client_timestamp > kUSER_LONGETH_SURVIVAL_TIME
    		    note over GatewayList: GatewayList_Remove(gateway_id)
    		    GatewayList->>ClientList: 3 ClientList_RemoveClientsConnectedOfflineGateway(gateway_id)
    		    loop
    		        alt client under this gateway
    		            note over ClientList: ClientList_Remove(client_id)
    		        end
    		    end
    		    
    		end
    	end
    	alt print_timestamp > kUSER_LIST_PRINT_TIME_INTERVAL
    		Heartbeat->>ClientList: 4 ClientList_Print()
    		Heartbeat->>GatewayList: 5 GatewayList_Print()
    		note over Heartbeat: reset print_timestamp
    	end
    end
~~~

