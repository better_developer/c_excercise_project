```mermaid
sequenceDiagram

participant StopModule
participant ClientSocketModule
participant ClientTimerModule
participant ChatModule
participant SendModule
participant MessageModule


note over StopModule: Receive SIGINT
note over StopModule: Start StopClient()


StopModule ->> SendModule: SendReleaseRequset()
SendModule ->> MessageModule: Pack client release request data
MessageModule ->> SendModule: Return packed client release request data
SendModule ->> ClientSocketModule: SendMsg()
ClientSocketModule ->> Remote: Send client release request

StopModule ->> ClientSocketModule: DeleteSocket()
StopModule ->> ClientTimerModule: DeleteClientTImers()
StopModule ->> ChatModule: DestroyList()
note over StopModule: Exit 0

```

```mermaid
sequenceDiagram




participant StopModule
participant ClientSocketModule
participant ClientTimerModule
participant ChatModule
participant ReceiveModule
participant Remote


Remote ->> ReceiveModule: Gateway release notification
ReceiveModule ->> StopModule: StopClientGatewayRelease()
StopModule ->> ClientSocketModule: DeleteSocket()
StopModule ->> ClientTimerModule: DeleteClientTImers()
StopModule ->> ChatModule: DestroyList()
note over StopModule: Exit 0


```



```