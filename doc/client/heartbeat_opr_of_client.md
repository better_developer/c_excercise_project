```mermaid
sequenceDiagram

participant ClientSocketModule
participant ClientTimerModule
participant MessageModule
participant SendModule
participant Remote

loop
    note over ClientTimerModule: HeartbeatTimer timeout
    ClientTimerModule ->> SendModule: SendHeartbeat()
    SendModule ->> MessageModule: Pack client heartbeat data
    MessageModule ->> SendModule: Return packed client heartbeat data
    SendModule ->> ClientSocketModule: SendMsg()
    ClientSocketModule ->> Remote: Send client heartbeat

end

```