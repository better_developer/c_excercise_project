```mermaid
sequenceDiagram


participant ChatModule
participant SendModule
participant MessageModule

participant ClientSocketModule
participant ReceiveModule
participant Remote
participant TargetClient


note over ChatModule: Input client id to chat
note over ChatModule: Input chat message
ChatModule ->> SendModule: SendChatMessage()
SendModule ->> MessageModule: Pack client chat message
MessageModule ->> SendModule: Return packed client chat message
SendModule ->> ClientSocketModule: SendMsg()
ClientSocketModule ->> Remote: Send client chat message

alt successful case

note over Remote: TargetClient online
Remote ->> TargetClient: Send client chat message

else TargetClient offline

Remote ->> ReceiveModule: Chat message response with kTARGET_CLIENT_OFFLINE_ERR
note over ReceiveModule: printf("Failed to send chat message!\n");

end

note over ChatModule: PrintChatMsgRecvBuf()

```

```mermaid
sequenceDiagram


participant ChatModule
participant ReceiveModule
participant Remote


Remote ->> ReceiveModule: Chat message
ReceiveModule ->> ChatModule: InsertMsg

```
