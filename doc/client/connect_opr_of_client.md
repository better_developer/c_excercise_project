```mermaid
sequenceDiagram

participant Client
participant ClientSocket
participant ClientTimer
participant Message
participant Listening
participant Remote


note over Client: Client_Init()
note over Client: Set kCLIENT_DISCONNECTED_STATUS

Client ->> ClientSocket: ClientSocket_Init()
note over ClientSocket: ClientSocket_Bind()
note over ClientSocket: ClientSocket_Connect()
Client ->> ClientTimer: ClientTimer_Init()

note over ClientTimer: Create Connection & Heartbeat timer
note over Client: signal(SIGINT, deinit);
note over Client: Client_StartConnection()
note over Client: inputClientId();
Client ->> Message: Message_SendConnectRequset()
Client ->> ClientTimer: ClientTimer_StartConnectionTimer()

Message ->> ClientSocket: ClientSocket_Send()
ClientSocket ->> Remote: Send client connection request

alt successful case

    Remote ->> Listening: Connect response kRESPONSE_STATUS_OK
    note over Listening: Get api id form message header
    note over Listening: receiveConnectionProcess()
    Listening ->> Message: Message_UnpackClientConnectResponseData()
    Listening ->> ClientTimer: ClientTimer_CloseConnectionTimer()
    Listening ->> ClientTimer: ClientTimer_StartHeartbeatTimer()
    Listening ->> Client: Client_SetConnectionStatus()


end






```