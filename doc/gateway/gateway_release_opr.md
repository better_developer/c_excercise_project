```mermaid
sequenceDiagram
    participant Client
    participant Gateway
    participant Server
    Note over Client,Server: Connection
    Note over Gateway: Gateway_Release()
    Gateway->>Server: 1 GATEWAT_RELEASE_REQUEST
    Gateway->>Client: 2 GATEWAY_RELEASE_NOTICE
    Note over Gateway: UserList_Deinit()
```
