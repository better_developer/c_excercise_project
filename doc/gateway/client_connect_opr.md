```mermaid
sequenceDiagram
    participant Client
    participant Listen
    participant Process
    participant UserList
    participant Server
    Client->>Listen: 1 CLIENT_CONNECTION_REQUEST
    Note over Listen: Unpack
    Listen->>Process: 2 Process_ClientLogin()
    Note over Process: Unpack
    Process->>UserList: 3 UserList_Find()
    Note over UserList: Search user list
    UserList->>Process: 4 client_info
    alt on the list
        Process->>Client: 5 kCONNECTED_ERR
    else absent
        Process->>UserList: 5 UserList_Add()
        note over Process: Pack the data with gatewayId
        Process->>Server: 6 CONNECTION_REQUEST_FORWARDING
        Server->>Listen: 7 CLIENT_CONNECTION_RESPONSE
        Note over Listen: Unpack
        Listen->>Process: 8 Process_ClientLoginResponse()
        Note over Process: Unpack
        Process->>UserList: 9 UserList_Find()
        Note over UserList: Search user list
        UserList->>Process: 10 client_info
        alt fail
            Process->>UserList: 11 UserList_Remove()
        end 
        Process->>Client: 12 CLIENT_CONNECTION_RESPONSE    
    end
```
