```mermaid
sequenceDiagram
    participant Client
    participant Listen
    participant Process
    participant UserList
    participant Server
    Client->>Listen: 1 CLIENT_RELEASE_REQUEST
    Note over Listen: Unpack
    Listen->>Process: 2 Process_ClientRelease()
    Note over Process: Unpack
    Process->>UserList: 3 UserList_Remove()
    Note over UserList: Remove client entry
    Process->>Server: 4 CLIENT_RELEASE_REQUEST
  
```
