```mermaid
sequenceDiagram
    participant Client
    participant Listen
    participant Process
    participant UserList
    participant Server
    participant TargetClient
    Client->>Listen: 1 CLIENT_SEND_CHAT_MESSAGE
    Note over Listen: Unpack
    Listen->>Process: 2 Process_ClientChat()
    Note over Process: Unpack
    Process->>UserList: 3 UserList_Find(target_id)
    Note over UserList: Search client in the userlist
    UserList->>Process: 4 client_info
    alt on the list
        Process->>TargetClient: 5 LIENT_SEND_CHAT_MESSAGE
        Process->>Client: 6 CHAT_MESSAGE_RESPONSE
    else absent
        Process->>Server: 7 LIENT_SEND_CHAT_MESSAGE
        Server->>Listen: 8 CHAT_MESSAGE_RESPONSE
        Listen->>Process: 9 Process_ClientChatResponse()
        Process->>Client: 10 CHAT_MESSAGE_RESPONSE
    end
```
