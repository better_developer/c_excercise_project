```mermaid
sequenceDiagram
    participant Gateway
    participant Timer
    participant UserList
    participant Process
    participant Listen
    participant Server
    Note over Gateway: Gateway_InputId()
    Note over Gateway: ateway_InitSocket()
    Note over UserList: UserList_Init()
    Note over Gateway: Gateway_InitConnection()
    Gateway->>Server: 1 GATEWAY_CONNECTION_REQUEST
    Gateway->>Timer: 2 Timer_Start()
    Note over Timer: Start timer
    alt successdul case
        Server->>Listen: 3 GATEWAY_CONNECTION_RESPONSE
        Note over Listen: unpack
        Listen->>Process: 4 Process_GatewayLoginResponse()
        Note over Process: unpack
        Process->>Timer: 5 Timer_DeleteTimer
        Note over Timer: Destroy timer
        Process->>Gateway: 6 Gateway_SetStatus()
    else gatewayID exited
        Server->>Listen: 3 GATEWAY_CONNECTION_RESPONSE
        Note over Listen: unpack
        Listen->>Process: 4 Process_GatewayLoginResponse()
        Note over Process: unpack
        Process->>Gateway: 5 Gateway_InputId()
        Note over Gateway: reset gatewatID
        Gateway->>Server: 6 GATEWAY_CONNECTION_REQUEST
    else No response
        loop
            Note over Timer: Timer expired
            Gateway->>Server: 3 GATEWAY_CONNECTION_REQUEST
        end
    end

```
