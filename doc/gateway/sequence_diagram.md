```mermaid
sequenceDiagram
    participant Client
    participant Gateway
    participant Server
    participant TargetClient
    Note over Client,Server: Connection
    Client->>Gateway: Send message
    Note over Gateway: Search user list
    alt on the list
        Gateway->>TargetClient: Forword Message
        TargetClient->>Gateway: Respond successful receipt
        Gateway->>Client: Respond successful receipt
    else absent
        Gateway->>Server: Forword Message
        Note over Server: Search user list
        rect rgb(255,228,181)
            alt on the list
                Server->>TargetClient: Forword Message
                TargetClient->>Server: Respond successful receipt
                Server->>Gateway: Respond successful receipt
                Gateway->>Client: Respond successful receipt
            else absent
                Server->>Gateway: Respond failure notice
                Gateway->>Client: Respond failure notice
            end    
        end 
            
    end
```
