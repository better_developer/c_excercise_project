```mermaid
sequenceDiagram
    participant Client
    participant Listen
    participant Process
    participant Gateway
    participant UserList
    participant Server
    Note over UserList: UserList_CheckHeartbeatTime()
    Gateway->>Server: Gateway_SendHeartbeat()
    alt 
        loop
            Client->>Listen: 1 CLIENT_HEARTBEAT
            Note over Listen: Unpack
            Listen->>Process: 2 Process_ClientHeartbeat()
            Note over Process: Unpack
            Process->>UserList: 3 UserList_UpdateHeartbeat()
            Note over UserList: Update heartbeat_time
        end  
    else timeout
        UserList->>Server: 1 CLIENT_RELEASE_NOTICE_FROM_GATEWAY
        Note over UserList: Delete client entry in the user list     
    end
```
