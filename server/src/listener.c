#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "listener.h"
#include "server_socket.h"
#include "msg_process_func.h"
#include "message_format.h"
#include "api.h"


void Listener_ProcessMsg(void)
{
    struct common_message_header *recv_msg_header = malloc(sizeof(struct common_message_header));
    struct sockaddr_in peer_addr;
    socklen_t peer_addr_len = sizeof(struct sockaddr_in);
    int sockfd = ServerSocket_GetSockfd();
    SInt8 recv_buff[kMSG_BUFFER_MAX_SIZE] = {0};
    UInt8 api_id = 0;

    printf("ready to receive...\n");
    while (1) {
        memset(recv_buff, 0, kMSG_BUFFER_MAX_SIZE);
        memset(&peer_addr, 0, sizeof(struct sockaddr_in));
        recvfrom(sockfd, recv_buff, kMSG_BUFFER_MAX_SIZE, 0, (struct sockaddr *)&peer_addr, &peer_addr_len);
        recv_msg_header = (struct common_message_header *)recv_buff;
        api_id = recv_msg_header->api_id;

        switch (api_id) {
        case CLIENT_CONNECTION_REQUEST_API_ID: {
            MsgProcessFunc_ProcessClientConnectionRequest(recv_buff, peer_addr);
            break;
        }
        case CONNECTION_REQUEST_FORWARDING_API_ID: {
            MsgProcessFunc_ProcessClientConnectionNotice(recv_buff, peer_addr);
            break;
        }
        case GATEWAY_CONNECTION_REQUEST_API_ID: {
            MsgProcessFunc_ProcessGatewayConnectionRequest(recv_buff, peer_addr);
            break;
        }
        case CLIENT_RELEASE_REQUEST_API_ID: {
            MsgProcessFunc_ProcessClientReleaseRequest(recv_buff);
            break;
        }
        case GATEWAY_RELEASE_REQUEST_API_ID: {
            MsgProcessFunc_ProcessGatewayReleaseRequest(recv_buff);
            break;
        }
        case CLIENT_RELEASE_NOTICE_FROM_GATEWAY_API_ID: {
            MsgProcessFunc_ProcessClientReleaseNotice(recv_buff);
            break;
        }
        case CLIENT_HEARTBEAT_API_ID: {
            MsgProcessFunc_ProcessClientHeartbeat(recv_buff);
            break;
        }
        case GATEWAY_HEARTBEAT_API_ID: {
            MsgProcessFunc_ProcessGatewayHeartbeat(recv_buff);
            break;
        }
        case CLIENT_SEND_CHAT_MESSAGE_API_ID: {
            MsgProcessFunc_ForwardChatMsg(recv_buff, peer_addr);
            break;
        }
        default:
            printf("\nudp packet type error.\n");
            break;
        }
    }
}