#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include "client_list.h"
#include "user_list.h"

static UserList client_list[kUSER_LIST_MAX_LENGTH];


void ClientList_Init(void)
{
    UserList_Init(client_list);
}


void ClientList_Deinit(void)
{
    UserList_Deinit(client_list);
}


static int getClientIndex(UInt16 client_id)
{
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (client_list[i].client_id == client_id) {
            return i;
        }
    }

    return kUSER_INDEX_ERROR;
}


int ClientList_Exist(UInt16 client_id)
{
    int search_flag = getClientIndex(client_id);

    if (search_flag == kUSER_INDEX_ERROR) {
        return USER_LIST_SEARCH_FAIL;
    }

    return USER_LIST_SEARCH_SUCCESS;
}


int ClientList_IsFull(void)
{
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (client_list[i].online_flag == USER_LIST_OFFLINE) {
            return USER_LIST_UNFULL;
        }
    }

    return USER_LIST_FULL;
}


void ClientList_Remove(UInt16 client_id)
{
    int index = getClientIndex(client_id);

    if (index == kUSER_INDEX_ERROR) {
        printf("Wrong client. Remove fail!\n");
        return;
    }

    client_list[index].client_id = 0;
    client_list[index].gateway_id = 0;
    client_list[index].online_flag = USER_LIST_OFFLINE;
    memset(&client_list[index].address, 0, sizeof(client_list[index].address));
    pthread_mutex_destroy(&client_list[index].mutex);
    printf("Remove client %d, it is offline.\n", client_id);
}


void ClientList_ResetTime(UInt16 client_id)
{
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (client_list[i].client_id == client_id && client_list[i].online_flag == USER_LIST_ONLINE) {
            pthread_mutex_lock(&client_list[i].mutex);
            gettimeofday(&client_list[i].survival_time_begin, NULL);
            pthread_mutex_unlock(&client_list[i].mutex);
            printf("Reset client %d heartbeat successfully.\n", client_id);
            break;
        }
    }
}


struct sockaddr_in ClientList_GetClientAddr(UInt16 client_id)
{
    int index = getClientIndex(client_id);
    struct sockaddr_in client_addr_null;

    memset(&client_addr_null, 0, sizeof(client_addr_null));
    if (index == kUSER_INDEX_ERROR) {
        printf("Fail to get client %d addres!\n", client_id);
        return client_addr_null;
    }

    return client_list[index].address;
}


UInt16 ClientList_GetGatewayId(UInt16 client_id)
{
    int index = getClientIndex(client_id);

    if (index == kUSER_INDEX_ERROR) {
        printf("Wrong client_id. Fail to get gateway_id of client %d connected !\n", client_id);
        return 0;
    }

    return client_list[index].gateway_id;
    printf("Get gateway_id of client %d connected successfully.\n", client_id);
}


int ClientList_Add(UInt16 client_id, UInt16 gateway_id, struct sockaddr_in address)
{
    return UserList_Insert(client_list, client_id, gateway_id, address);
}


void ClientList_Update(void)
{
    int i, time_interval = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (client_list[i].online_flag == USER_LIST_OFFLINE || client_list[i].gateway_id > 0) {
            continue;
        }
        pthread_mutex_lock(&client_list[i].mutex);
        gettimeofday(&client_list[i].survival_time_end, NULL);
        pthread_mutex_unlock(&client_list[i].mutex);

        time_interval = client_list[i].survival_time_end.tv_sec - client_list[i].survival_time_begin.tv_sec;
        if (time_interval > kUSER_LONGETH_SURVIVAL_TIME) {
            ClientList_Remove(client_list[i].client_id);
        }
    }
}


void ClientList_Print(void)
{
    int i = 0;

    printf("\n******************online client*****************\n");
    printf("ClientID    Connected_GatewayID    Address\n");
    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (client_list[i].online_flag == USER_LIST_ONLINE){
            printf("%-12d%-23d%-10d\n", client_list[i].client_id, client_list[i].gateway_id, client_list[i].address.sin_addr.s_addr);
        }
    }
    printf("\n************************************************\n");
}


void ClientList_RemoveClientsConnectedOfflineGateway(UInt16 gateway_id)
{
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (client_list[i].gateway_id == gateway_id && client_list[i].online_flag == USER_LIST_ONLINE) {
            ClientList_Remove(client_list[i].client_id);
        }
    }
}