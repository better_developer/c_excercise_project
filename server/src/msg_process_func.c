#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "msg_process_func.h"
#include "user_list.h"
#include "client_list.h"
#include "gateway_list.h"
#include "server_socket.h"
#include "api.h"
#include "error_code.h"
#include "response_status.h"
#include "message_format.h"


struct sockaddr_in getTargetAddr(UInt16 target_id)
{
    UInt16 gateway_id = ClientList_GetGatewayId(target_id);
    struct sockaddr_in client_addr;

    memset(&client_addr, 0, sizeof(client_addr));
    if (gateway_id == 0) {
        return ClientList_GetClientAddr(target_id);
    }

    return GatewayList_GetGatewayAddr(gateway_id);
}


static void sendConnectionResponse(UInt8 api_id, UInt8 err_code, void *recv_buff, struct sockaddr_in peer_addr)
{
    struct common_reponse connection_request_response;
    struct common_info *connection_request = recv_buff;
    int sockfd = ServerSocket_GetSockfd();
    int send_ret;

    connection_request_response.header.api_id = api_id;
    connection_request_response.body.client_id_or_gateway_id = connection_request->body.client_id_or_gateway_id;
    connection_request_response.body.error_code = err_code;
    connection_request_response.body.status = (err_code == 0) ? kRESPONSE_STATUS_OK : kRESPONSE_STATUS_ERROR;

    send_ret = sendto(sockfd, &connection_request_response, sizeof(connection_request_response), 0, (const struct sockaddr *)&peer_addr, sizeof(peer_addr));
    printf("%s", (send_ret < 0) ? "fail to send response\n" : "Send connection response successfully.\n");
}


static void processFullClientListCase(void *recv_buff, struct sockaddr_in client_addr)
{
    UInt8 err_code = CONNECTED_ERR_OF_LIST_FULL;

    printf("The client list is full.\n");
    sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, client_addr);
}


static void processClientExistCase(UInt16 client_id, void *recv_buff, struct sockaddr_in peer_addr)
{
    UInt8 err_code = CONNECTED_ERR_OF_ID_EXISTED;
    struct sockaddr_in tmp_addr;

    tmp_addr = getTargetAddr(client_id);
    if (tmp_addr.sin_addr.s_addr != 0) {
        printf("This client_id %d exists already.\n", client_id);
        sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, peer_addr);
    }
}


static void processSuccessfulClientConn(UInt16 client_id, UInt16 gateway_id, void *recv_buff, struct sockaddr_in user_addr)
{
    int add_flag;
    UInt8 err_code = 0;
    struct sockaddr_in client_addr;

    memset(&client_addr, 0, sizeof(client_addr));
    add_flag = (gateway_id == 0) ? ClientList_Add(client_id, gateway_id, user_addr) : ClientList_Add(client_id, gateway_id, client_addr);
    err_code = (add_flag == USER_LIST_ADD_SUCCESS) ? 0 : CONNECTED_ERR_OF_ADD_UNSUCCESSFULLY;
    printf("Add client %d %s!\n", client_id, (err_code == 0) ? "successfully" : "unsuccessfully");

    sendConnectionResponse(CLIENT_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, user_addr);
}


void MsgProcessFunc_ProcessClientConnectionRequest(void *recv_buff, struct sockaddr_in client_addr)
{
    struct common_info *client_connection_request = recv_buff;
    UInt16 client_id = 0;

    assert(recv_buff);
    client_id = ntohs(client_connection_request->body.client_id_or_gateway_id);

    if (client_id <= 0) {
        return;
    }
    printf("\nReceive a connection request from client %d.\n", client_id);
    if (ClientList_IsFull()) {
        processFullClientListCase(recv_buff, client_addr);
        return;
    }
    if (ClientList_Exist(client_id)) {
        processClientExistCase(client_id, recv_buff, client_addr);
        return;
    }
    processSuccessfulClientConn(client_id, 0, recv_buff, client_addr);
}


void MsgProcessFunc_ProcessClientConnectionNotice(void *recv_buff, struct sockaddr_in gateway_addr)
{
    struct client_connect_requset_forwarding *client_connection_request_notice = recv_buff;
    UInt16 client_id, gateway_id;

    assert(recv_buff);
    client_id = ntohs(client_connection_request_notice->body.client_id);
    gateway_id = ntohs(client_connection_request_notice->body.gateway_id);

    if (client_id <= 0) {
        return;
    }
    printf("\nRecv client %d connection notice from gateway %d\n", client_id, gateway_id);
    if (ClientList_IsFull()) {
        processFullClientListCase(recv_buff, gateway_addr);
        return;
    }
    if (ClientList_Exist(client_id)) {
        processClientExistCase(client_id, recv_buff, gateway_addr);
        return;
    }
    processSuccessfulClientConn(client_id, gateway_id, recv_buff, gateway_addr);
}


static void processFullGatewayListCase(void *recv_buff, struct sockaddr_in gateway_addr)
{
    UInt8 err_code = CONNECTED_ERR_OF_LIST_FULL;

    printf("The gateway list is full.\n");
    sendConnectionResponse(GATEWAY_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr);
}


static void processGatewayExistCase(UInt16 gateway_id, void *recv_buff, struct sockaddr_in gateway_addr)
{
    UInt8 err_code = CONNECTED_ERR_OF_ID_EXISTED;

    printf("This gateway_id exists already.\n");
    sendConnectionResponse(GATEWAY_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr);
}


static void processSuccessfulGatewayConn(UInt16 gateway_id, void *recv_buff, struct sockaddr_in gateway_addr)
{
    int add_flag = GatewayList_Add(gateway_id, gateway_addr);
    UInt8 err_code = 0;

    err_code = (add_flag == USER_LIST_ADD_SUCCESS) ? 0 : CONNECTED_ERR_OF_ADD_UNSUCCESSFULLY;
    printf("Add gateway %d %s!\n", gateway_id, (err_code == 0) ? "successfully" : "unsuccessfully");
    sendConnectionResponse(GATEWAY_CONNECTION_RESPONSE_API_ID, err_code, recv_buff, gateway_addr);
}


void MsgProcessFunc_ProcessGatewayConnectionRequest(void *recv_buff, struct sockaddr_in gateway_addr)
{
    struct common_info *gateway_connection_request = recv_buff;
    UInt16 gateway_id = 0;

    assert(recv_buff);
    gateway_id = ntohs(gateway_connection_request->body.client_id_or_gateway_id);

    if (gateway_id <= 0) {
        return;
    }
    printf("\nReceive a connection request from gateway %d.\n", gateway_id);
    if (GatewayList_IsFull()) {
        printf("enter if (GatewayList_IsFull()) {\n");
        processFullGatewayListCase(recv_buff, gateway_addr);
        printf("quit if (GatewayList_IsFull()) {\n");
        return;
    }
    if (GatewayList_Exist(gateway_id)) {
        printf("enter if (GatewayList_Exist(gateway_id)) {\n");
        processGatewayExistCase(gateway_id, recv_buff, gateway_addr);
        printf("quit if (GatewayList_Exist(gateway_id)) {\n");
        return;
    }
    processSuccessfulGatewayConn(gateway_id, recv_buff, gateway_addr);
}


void MsgProcessFunc_ProcessClientReleaseRequest(void *recv_buff)
{
    struct common_info *client_release_request = recv_buff;
    UInt16 client_id;

    assert(recv_buff);
    client_id = ntohs(client_release_request->body.client_id_or_gateway_id);
    printf("\nReceive a release request from client %d.\n", client_id);
    ClientList_Remove(client_id);
}


void MsgProcessFunc_ProcessClientReleaseNotice(void *recv_buff)
{
    struct client_release_notice_from_gateway *client_release_notice = recv_buff;
    UInt16 client_id;

    assert(recv_buff);
    client_id = ntohs(client_release_notice->body.client_id);
    printf("\nReceive client %d release notice from gateway", client_id);
    ClientList_Remove(client_id);
}


void MsgProcessFunc_ProcessGatewayReleaseRequest(void *recv_buff)
{
    struct common_info *gateway_release_request = recv_buff;
    UInt16 gateway_id;

    assert(recv_buff);
    gateway_id = ntohs(gateway_release_request->body.client_id_or_gateway_id);
    printf("\nReceive a release request from gateway %d.\n", gateway_id);
    GatewayList_Remove(gateway_id);
    ClientList_RemoveClientsConnectedOfflineGateway(gateway_id);
}


static void sendForwardingResponse(int forward_status, void *recv_buff, struct sockaddr_in peer_addr)
{
    struct common_reponse chat_msg_response;
    struct chat_message_from_client *chat_msg = recv_buff;
    int sockfd = ServerSocket_GetSockfd();
    int send_ret;

    chat_msg_response.header.api_id = CHAT_MESSAGE_RESPONSE_API_ID;
    chat_msg_response.body.client_id_or_gateway_id = chat_msg->body.source_client_id;
    chat_msg_response.body.error_code = (forward_status > 0) ? 0 : TARGET_CLIENT_OFFLINE_ERR;
    chat_msg_response.body.status = (forward_status > 0) ? kRESPONSE_STATUS_OK : kRESPONSE_STATUS_ERROR;

    send_ret = sendto(sockfd, &chat_msg_response, sizeof(chat_msg_response), 0, (const struct sockaddr *)&peer_addr, sizeof(peer_addr));
    printf("%s", (send_ret < 0) ? "fail to response chat_msg_forwarding\n" : "Send chat message forwarding response successfully.\n");
}


void MsgProcessFunc_ForwardChatMsg(void *recv_buff, struct sockaddr_in peer_addr)
{
    struct chat_message_from_client *chat_msg = recv_buff;
    struct sockaddr_in target_addr;
    UInt16 target_client_id = 0;
    int sockfd = ServerSocket_GetSockfd();
    int forward_status = 0;

    assert(recv_buff);
    target_client_id = ntohs(chat_msg->body.destination_client_id);
    target_addr = getTargetAddr(target_client_id);

    printf("\nclient %d send a message to client %d\n", ntohs(chat_msg->body.source_client_id), target_client_id);
    forward_status = sendto(sockfd, chat_msg, sizeof(struct chat_message_from_client) + ntohs(chat_msg->body.chat_msg_len), 0, (const struct sockaddr *)&target_addr, sizeof(target_addr));
    printf("%s", (forward_status < 0) ? "fail to forward chat_mag\n" : "Forward chat message successfully.\n");
    sendForwardingResponse(forward_status, recv_buff, peer_addr);
}


void MsgProcessFunc_ProcessClientHeartbeat(void *recv_buff)
{
    struct common_info *client_heartbeat = recv_buff;
    UInt16 client_id;

    client_id = ntohs(client_heartbeat->body.client_id_or_gateway_id);
    printf("\nReceive client %d heartbeat packet.\n", client_id);
    if (!ClientList_Exist(client_id)) {
        printf("This client %d doesn't exist.\n", client_id);
        return;
    }
    ClientList_ResetTime(client_id);
}


void MsgProcessFunc_ProcessGatewayHeartbeat(void *recv_buff)
{
    struct common_info *gateway_heartbeat = recv_buff;
    UInt16 gateway_id;

    gateway_id = ntohs(gateway_heartbeat->body.client_id_or_gateway_id);
    printf("\nReceive gateway %d heartbeat packet.\n", gateway_id);
    if (!GatewayList_Exist(gateway_id)) {
        printf("This gateway %d doesn't exist.\n", gateway_id);
        return;
    }
    GatewayList_ResetTime(gateway_id);
}