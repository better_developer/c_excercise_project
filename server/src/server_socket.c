#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "server_socket.h"


static ServerSocket server_socket;


void ServerSocket_Init(void)
{
    int bind_msg;

    server_socket.sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    server_socket.address.sin_family = AF_INET;
    server_socket.address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_socket.address.sin_port = htons(kSERVER_PORT);

    bind_msg = bind(server_socket.sockfd, (struct sockaddr *)&server_socket.address, sizeof(server_socket.address));
    if (bind_msg) {
        printf("Socket bind fail!\n");
        exit(-1);
    }
    printf("bind successfully!\n");
}


int ServerSocket_GetSockfd(void)
{
    return server_socket.sockfd;
}


void ServerSocket_Deinit()
{
    close(server_socket.sockfd);
}