#include <stddef.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include "heartbeat.h"
#include "user_list.h"
#include "client_list.h"
#include "gateway_list.h"


void *Heartbeat_UpdateUserStatus(void *args)
{
    struct timeval time_begin, time_end;

    gettimeofday(&time_begin, NULL);
    while (1) {
        ClientList_Update();
        GatewayList_Update();
        
        gettimeofday(&time_end, NULL);
        if (time_end.tv_sec - time_begin.tv_sec >= kUSER_LIST_PRINT_TIME_INTERVAL) {
            ClientList_Print();
            GatewayList_Print();
            gettimeofday(&time_begin, NULL);
        }
    }

    return NULL;
}