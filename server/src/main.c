#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include "listener.h"
#include "heartbeat.h"
#include "client_list.h"
#include "gateway_list.h"
#include "server_socket.h"


static void deinitServer()
{
    printf("\nGet SIGINT signal. Server offline!\n\n");
    ServerSocket_Deinit();
    ClientList_Deinit();
    GatewayList_Deinit();

    exit(0);
}


int main(void)
{
    pthread_t heartbeat_thread;

    signal(SIGINT, deinitServer);

    ClientList_Init();
    GatewayList_Init();
    ServerSocket_Init();

    pthread_create(&heartbeat_thread, NULL, Heartbeat_UpdateUserStatus, NULL);
    Listener_ProcessMsg();

    return 0;
}
