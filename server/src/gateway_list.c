#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include "gateway_list.h"
#include "user_list.h"
#include "client_list.h"


static UserList gateway_list[kUSER_LIST_MAX_LENGTH];


void GatewayList_Init(void)
{
    UserList_Init(gateway_list);
}


void GatewayList_Deinit(void)
{
    UserList_Deinit(gateway_list);
}


static int getGatewayIndex(UInt16 gateway_id)
{
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (gateway_list[i].gateway_id == gateway_id) {
            return i;
        }
    }

    return kUSER_INDEX_ERROR;
}


int GatewayList_Exist(UInt16 gateway_id)
{
    printf("running GatewayList_Exist\n");
    int search_flag = getGatewayIndex(gateway_id);

    if (search_flag == kUSER_INDEX_ERROR) {
        return USER_LIST_SEARCH_FAIL;
    }

    return USER_LIST_SEARCH_SUCCESS;
}


int GatewayList_IsFull(void)
{
    printf("Running GatewayList_IsFull\n");
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (gateway_list[i].online_flag == USER_LIST_OFFLINE) {
            return USER_LIST_UNFULL;
        }
    }

    return USER_LIST_FULL;
}


int GatewayList_Add(UInt16 gateway_id, struct sockaddr_in address)
{
    return UserList_Insert(gateway_list, 0, gateway_id, address);
}


void GatewayList_Remove(UInt16 gateway_id)
{
    int index = getGatewayIndex(gateway_id);

    if (index == kUSER_INDEX_ERROR) {
        printf("Wrong gateway. Remove fail!\n");
        return;
    }

    gateway_list[index].gateway_id = 0;
    gateway_list[index].online_flag = USER_LIST_OFFLINE;
    memset(&gateway_list[index].address, 0, sizeof(gateway_list[index].address));
    pthread_mutex_destroy(&gateway_list[index].mutex);
    printf("Remove gateway %d, it is offline.\n", gateway_id);
}


void GatewayList_Print(void)
{
    int i = 0;

    printf("\n******************online gateway*****************\n");
    printf("GatewayID    Address\n");
    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (gateway_list[i].online_flag == USER_LIST_ONLINE) {
            printf("%-13d%-10d\n", gateway_list[i].gateway_id, gateway_list[i].address.sin_addr.s_addr);
        }
    }
    printf("*************************************************\n\n");
}


void GatewayList_Update(void)
{
    int i, time_interval = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (gateway_list[i].online_flag == USER_LIST_OFFLINE) {
            continue;
        }
        pthread_mutex_lock(&gateway_list[i].mutex);
        gettimeofday(&gateway_list[i].survival_time_end, NULL);
        pthread_mutex_unlock(&gateway_list[i].mutex);

        time_interval = gateway_list[i].survival_time_end.tv_sec - gateway_list[i].survival_time_begin.tv_sec;
        if (time_interval > kUSER_LONGETH_SURVIVAL_TIME) {
            ClientList_RemoveClientsConnectedOfflineGateway(gateway_list[i].gateway_id);
            GatewayList_Remove(gateway_list[i].gateway_id);  
        }
    }
}


void GatewayList_ResetTime(UInt16 gateway_id)
{
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (gateway_list[i].gateway_id == gateway_id && gateway_list[i].online_flag == USER_LIST_ONLINE) {
            pthread_mutex_lock(&gateway_list[i].mutex);
            gettimeofday(&gateway_list[i].survival_time_begin, NULL);
            pthread_mutex_unlock(&gateway_list[i].mutex);
            printf("Reset gateway %d heartbeat successfully.\n", gateway_id);
            break;
        }
    }
}


struct sockaddr_in GatewayList_GetGatewayAddr(UInt16 gateway_id)
{
    int index = getGatewayIndex(gateway_id);
    struct sockaddr_in gateway_addr_null;

    memset(&gateway_addr_null, 0, sizeof(gateway_addr_null));
    if (index == kUSER_INDEX_ERROR) {
        printf("Get gateway address unsuccessfully!\n");
        return gateway_addr_null;
    }
    return gateway_list[index].address;
}