#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>
#include "user_list.h"


void UserList_Init(UserList *user_list)
{
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        user_list[i].client_id = 0;
        user_list[i].gateway_id = 0;
        user_list[i].online_flag = USER_LIST_OFFLINE;
        memset(&user_list[i].address, 0, sizeof(user_list[i].address));
        pthread_mutex_init(&user_list[i].mutex, NULL);
    }
}


void UserList_Deinit(UserList *user_list)
{
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        user_list[i].client_id = 0;
        user_list[i].gateway_id = 0;
        memset(&user_list[i].address, 0, sizeof(user_list[i].address));
        pthread_mutex_destroy(&user_list[i].mutex);
    }
}


int UserList_Insert(UserList *user_list, UInt16 client_id, UInt16 gateway_id, struct sockaddr_in address)
{
    int i = 0;

    for (i=0; i<kUSER_LIST_MAX_LENGTH; i++) {
        if (user_list[i].online_flag == USER_LIST_ONLINE) {
            continue;
        }
        user_list[i].client_id = client_id;
        user_list[i].gateway_id = gateway_id;
        user_list[i].online_flag = USER_LIST_ONLINE;
        user_list[i].address = address;
        gettimeofday(&user_list[i].survival_time_begin, NULL);
        pthread_mutex_init(&user_list[i].mutex, NULL);

        return USER_LIST_ADD_SUCCESS;
    }

    return USER_LIST_ADD_FAIL;
}