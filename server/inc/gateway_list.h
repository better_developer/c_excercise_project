#pragma once
#include <netinet/in.h>
#include "type.h"

void GatewayList_Init(void);
void GatewayList_Deinit(void);
int GatewayList_Exist(UInt16 gateway_id);
int GatewayList_IsFull(void);
int GatewayList_Add(UInt16 gateway_id, struct sockaddr_in address);
void GatewayList_Remove(UInt16 gateway_id);
void GatewayList_Print(void);
void GatewayList_Update(void);
void GatewayList_ResetTime(UInt16 gateway_id);
struct sockaddr_in GatewayList_GetGatewayAddr(UInt16 gateway_id);