#pragma once
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include "type.h"

#define kUSER_LIST_MAX_LENGTH 5
#define kUSER_LIST_PRINT_TIME_INTERVAL 10
#define kUSER_LONGETH_SURVIVAL_TIME 30
#define kUSER_INDEX_ERROR -1

enum {
    USER_LIST_ONLINE,
    USER_LIST_OFFLINE
};

enum {
    USER_LIST_SEARCH_FAIL,
    USER_LIST_SEARCH_SUCCESS,
};

enum {
    USER_LIST_ADD_FAIL,
    USER_LIST_ADD_SUCCESS
};

enum {
    USER_LIST_UNFULL,
    USER_LIST_FULL
};

typedef struct {
    UInt16 client_id;
    UInt16 gateway_id;
    UInt16 remaining_survival_time;
    int online_flag;
    struct timeval survival_time_begin;
    struct timeval survival_time_end;
    struct sockaddr_in address;
    pthread_mutex_t mutex;
}UserList;

void UserList_Init(UserList *user_list);
void UserList_Deinit(UserList *user_list);
int UserList_Insert(UserList *user_list, UInt16 client_id, UInt16 gateway_id, struct sockaddr_in address);
