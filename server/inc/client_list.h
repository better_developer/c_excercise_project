#pragma once
#include <netinet/in.h>
#include "type.h"

void ClientList_Init(void);
void ClientList_Deinit(void);
int ClientList_Exist(UInt16 client_id);
int ClientList_IsFull(void);
int ClientList_Add(UInt16 client_id, UInt16 gateway_id, struct sockaddr_in address);
void ClientList_Remove(UInt16 client_id);
void ClientList_Print(void);
void ClientList_Update(void);
void ClientList_ResetTime(UInt16 client_id);
UInt16 ClientList_GetGatewayId(UInt16 client_id);
struct sockaddr_in ClientList_GetClientAddr(UInt16 client_id);
void ClientList_RemoveClientsConnectedOfflineGateway(UInt16 gateway_id);