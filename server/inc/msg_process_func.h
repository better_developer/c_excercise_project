#pragma once
#include <netinet/in.h>
#include "type.h"

#define kMSG_BUFFER_MAX_SIZE 1024

void MsgProcessFunc_ProcessClientConnectionRequest(void *recv_buff, struct sockaddr_in client_addr);
void MsgProcessFunc_ProcessClientConnectionNotice(void *recv_buff, struct sockaddr_in gateway_addr);
void MsgProcessFunc_ProcessGatewayConnectionRequest(void *recv_buff, struct sockaddr_in gateway_addr);
void MsgProcessFunc_ProcessClientReleaseRequest(void *recv_buff);
void MsgProcessFunc_ProcessClientReleaseNotice(void *recv_buff);
void MsgProcessFunc_ProcessGatewayReleaseRequest(void *recv_buff);
void MsgProcessFunc_ForwardChatMsg(void *recv_buff, struct sockaddr_in peer_addr);
void MsgProcessFunc_SendForwardingResponse(int forward_status, void *recv_buff, struct sockaddr_in peer_addr);
void MsgProcessFunc_ProcessClientHeartbeat(void *recv_buff);
void MsgProcessFunc_ProcessGatewayHeartbeat(void *recv_buff);