#pragma once
#include <sys/socket.h>
#include <netinet/in.h>

#define kSERVER_PORT 8888

typedef struct {
    int sockfd;
    struct sockaddr_in address;
}ServerSocket;

void ServerSocket_Init(void);
int ServerSocket_GetSockfd(void);
void ServerSocket_Deinit();