import struct
from constants import *
from mysocket import *
from common_func import *
from client_python import *


class Gateway():
    def __init__(self):
        self.gateway = UdpSocket()
        self.gateway_id = 0
        self.gateway_status = USER_OFFLINE


    def gateway_init(self, host, port):
        self.gateway.socket_init(host, port)


    def gateway_deinit(self):
        if self.gateway_status == USER_OFFLINE:
            self.gateway.socket_close()
        else:
            self.gateway_send_release_request()


    def gateway_send_conn_request(self, gateway_id):
        send_msg = pack_gateway_conn_request(gateway_id)
        self.gateway.send(send_msg, server_host, server_port)
        self.gateway_id = gateway_id


    def gateway_recv_conn_response(self):
        while True:
            recv_data = self.gateway.receive()
            api_id = unpack_header(recv_data)
            if api_id == GATEWAY_CONNECTION_RESPONSE_API_ID:
                gateway_status = unpack_gateway_conn_response(recv_data)[1]
                if gateway_status == RESPONSE_STATUS_OK:
                    self.gateway_status = USER_ONLINE
                return recv_data


    def gateway_send_client_conn_notice(self, client_id):
        send_msg = pack_client_conn_notice(client_id, self.gateway_id)
        self.gateway.send(send_msg, server_host, server_port)


    def gateway_recv_client_conn_response(self):
        while True:
            recv_data = self.gateway.receive()
            api_id = unpack_header(recv_data)
            if api_id == CLIENT_CONNECTION_RESPONSE_API_ID:
                return recv_data


    def gateway_send_release_request(self):
        send_msg = pack_gateway_release_request(self.gateway_id)
        self.gateway.send(send_msg, server_host, server_port)
        self.gateway_status = USER_OFFLINE
        self.gateway.socket_close()


    def gateway_send_err_release_request(self, err_gateway_id):
        send_msg = pack_gateway_release_request(err_gateway_id)
        self.gateway.send(send_msg, server_host, server_port)


    def gateway_send_heartbeat(self):
        send_msg = pack_gateway_heartbeat(self.gateway_id)
        self.gateway.send(send_msg, server_host, server_port)


    def gateway_send_heartbeat_err(self, gateway_id):
        send_msg = pack_gateway_heartbeat(gateway_id)
        self.gateway.send(send_msg, server_host, server_port)


    def gateway_send_client_release_notice(self, client_id):
        send_msg = pack_client_release_notice(client_id)
        self.gateway.send(send_msg, server_host, server_port)


    def gateway_forward_chat_msg_to_server(self, src_id, dest_id, msg_len, msg):
        send_msg = pack_chat_msg(src_id, dest_id, msg_len, msg)
        self.gateway.send(send_msg, server_host, server_port)


    def gateway_recv_chat_msg_from_server(self):
        while True:
            recv_data = self.gateway.receive()
            api_id = unpack_header(recv_data)
            if api_id == CLIENT_SEND_CHAT_MESSAGE_API_ID:
                return recv_data


    def gateway_recv_chat_msg_response_from_server(self):
        while True:
            recv_data = self.gateway.receive()
            api_id = unpack_header(recv_data)
            if api_id == CHAT_MESSAGE_RESPONSE_API_ID:
                return recv_data


def unpack_gateway_conn_response(data):
    gateway_id, conn_status = struct.unpack("!BHB", data[0:4])[1:3]
    return gateway_id, conn_status


def pack_gateway_conn_request(gateway_id):
    send_msg = struct.pack("!BH", GATEWAY_CONNECTION_REQUEST_API_ID, gateway_id)
    return send_msg


def pack_gateway_heartbeat(gateway_id):
    send_msg = struct.pack("!BH", GATEWAY_HEARTBEAT_API_ID, gateway_id)
    return send_msg


def pack_client_conn_notice(client_id, gateway_id):
    send_msg = struct.pack("!BHH", CONNECTION_REQUEST_FORWARDING_API_ID, client_id, gateway_id)
    return send_msg


def pack_gateway_release_request(gateway_id):
    send_msg = struct.pack("!BH", GATEWAY_RELEASE_REQUEST_API_ID, gateway_id)
    return send_msg


def pack_client_release_notice(client_id):
    send_msg = struct.pack("!BH", CLIENT_RELEASE_NOTICE_FROM_GATEWAY_API_ID, client_id)
    return send_msg


def pack_gateway_conn_reponse_with_full_list(gateway_id):
    data = struct.pack("!BHBB", GATEWAY_CONNECTION_RESPONSE_API_ID, gateway_id, RESPONSE_STATUS_ERROR, CONNECTED_ERR_OF_LIST_FULL)
    return data


def create_expected_conn_response_of_client_conn_notice_with_full_list(n):
    conn_responses = []
    for i in range(n-1):
        conn_responses.append(pack_client_conn_response_with_success(i+1))
    conn_responses.append(pack_client_conn_response_with_full_list(n))
    return conn_responses


def create_expected_gateway_conn_reponse_of_full_list_case(n):
    expected_gateway_conn_reponses = []
    for i in range(n-1):
        expected_gateway_conn_reponses.append(pack_gateway_conn_response_with_success(i+1))
    expected_gateway_conn_reponses.append(pack_gateway_conn_reponse_with_full_list(n+1))
    return expected_gateway_conn_reponses


def pack_gateway_conn_reponse_with_existed_id(gateway_id):
    data = struct.pack("!BHBB", GATEWAY_CONNECTION_RESPONSE_API_ID, gateway_id, RESPONSE_STATUS_ERROR, CONNECTED_ERR_OF_ID_EXISTED)
    return data


def pack_gateway_conn_response_with_success(gateway_id):
    data = struct.pack("!BHBB", GATEWAY_CONNECTION_RESPONSE_API_ID, gateway_id, RESPONSE_STATUS_OK, 0)
    return data