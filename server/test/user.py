from constants import *
from client_python import *
from gateway_python import *

Clients = []
Gateways = []


def create_clients(n):
    for i in range(n):
        tmp = Client()
        tmp.client_init(LOCAL_HOST, base_client_port+i)
        Clients.append(tmp)


def release_all_clients(n):
    for i in range(n):
        try:
            Clients[i].client_deinit()
        except:
            continue
    Clients.clear()


def create_gateways(n):
    for i in range(n):
        tmp = Gateway()
        tmp.gateway_init(LOCAL_HOST, gateway_port+i)
        Gateways.append(tmp)


def release_all_gateways(n):
    for i in range(n):
        try:
            Gateways[i].gateway_deinit()
        except:
            continue
    Gateways.clear()