import socket
from constants import *

class UdpSocket(object):

    def socket_init(self, host, port):
        self.sockfd = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sockfd.bind((host, port))
    
    def socket_close(self):
        self.sockfd.close()
        self.sockfd = None
    
    def receive(self):
        recv_msg, addr = self.sockfd.recvfrom(RECV_BUFF_SIZE)
        return recv_msg
    
    def send(self, send_msg, host, port):
        self.sockfd.sendto(send_msg, (host, port))