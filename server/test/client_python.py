import struct
import time
from constants import *
from mysocket import *
from common_func import *


class Client():
    def __init__(self):
        self.client = UdpSocket()
        self.client_id = 0
        self.client_status = USER_OFFLINE


    def client_init(self, host, port):
        self.client.socket_init(host, port)


    def client_deinit(self):
        if self.client_status == USER_OFFLINE:
            self.client.socket_close()
        else:
            self.client_send_release_request()


    def client_send_conn_request(self, client_id):
        send_msg = pack_client_conn_request(client_id)
        self.client.send(send_msg, server_host, server_port)
        self.client_id = client_id


    def client_recv_conn_response(self):
        while True:
            recv_data = self.client.receive()
            api_id = unpack_header(recv_data)
            if api_id == CLIENT_CONNECTION_RESPONSE_API_ID:
                conn_status = unpack_client_conn_response(recv_data)[1]
                if conn_status == RESPONSE_STATUS_OK:
                    self.client_status = USER_ONLINE
                return recv_data


    def client_send_heartbeat(self):
        send_msg = pack_client_heartbeat(self.client_id)
        self.client.send(send_msg, server_host, server_port)
    

    def client_send_heartbeat_err(self, client_id):
        send_msg = pack_client_heartbeat(client_id)
        self.client.send(send_msg, server_host, server_port)


    def client_send_release_request(self):
        send_msg = pack_client_release_request(self.client_id)
        self.client.send(send_msg, server_host, server_port)
        self.client_status = USER_OFFLINE
        self.client.socket_close()
    

    def client_send_err_release_request(self, err_client_id):
        send_msg = pack_client_release_request(err_client_id)
        self.client.send(send_msg, server_host, server_port)


    def client_send_chat_msg(self, src_id, dest_id, msg_len, msg):
        send_msg = pack_chat_msg(src_id, dest_id, msg_len, msg)
        self.client.send(send_msg, server_host, server_port)


    def client_recv_chat_msg(self):
        while True:
            recv_data = self.client.receive()
            api_id = unpack_header(recv_data)
            if api_id == CLIENT_SEND_CHAT_MESSAGE_API_ID:
                return recv_data


    def client_recv_chat_msg_response(self):
        while True:
            recv_data = self.client.receive()
            api_id = unpack_header(recv_data)
            if api_id == CHAT_MESSAGE_RESPONSE_API_ID:
                return recv_data
    

    def client_send_err_api(self, api_id):
        send_msg = struct.pack("!BH", api_id, self.client_id)
        self.client.send(send_msg, server_host, server_port)


def unpack_client_conn_response(data):
    client_id, conn_status = struct.unpack("!BHB", data[0:4])[1:3]
    return client_id, conn_status


def pack_client_conn_request(client_id):
    send_msg = struct.pack("!BH", CLIENT_CONNECTION_REQUEST_API_ID, client_id)
    return send_msg


def pack_client_heartbeat(client_id):
    send_msg = struct.pack("!BH", CLIENT_HEARTBEAT_API_ID, client_id)
    return send_msg


def pack_client_release_request(client_id):
    send_msg = struct.pack("!BH", CLIENT_RELEASE_REQUEST_API_ID, client_id)
    return send_msg


def pack_client_conn_response_with_full_list(client_id):
    data = struct.pack("!BHBB", CLIENT_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_ERROR, CONNECTED_ERR_OF_LIST_FULL)
    return data


def create_expected_client_conn_response_of_full_list_case(n):
    expected_recv_conn_responses = []
    for i in range(n-1):
        expected_recv_conn_responses.append(pack_client_conn_response_with_success(i+1))
    expected_recv_conn_responses.append(pack_client_conn_response_with_full_list(n+1))
    return expected_recv_conn_responses


def pack_client_conn_reponse_with_existed_id(client_id):
    data = struct.pack("!BHBB", CLIENT_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_ERROR, CONNECTED_ERR_OF_ID_EXISTED)
    return data


def pack_client_conn_response_with_success(client_id):
    data = struct.pack("!BHBB", CLIENT_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_OK, 0)
    return data


def pack_chat_msg_forward_successfully_response(id):
    data = struct.pack("!BHBB", CHAT_MESSAGE_RESPONSE_API_ID, id, RESPONSE_STATUS_OK, 0)
    return data


def pack_chat_msg_forward_unsuccessfully_response(id):
    data = struct.pack("!BHBB", CHAT_MESSAGE_RESPONSE_API_ID, id, RESPONSE_STATUS_ERROR, TARGET_CLIENT_OFFLINE_ERR)
    return data


def client_send_heartbeat_continuously(client, time_interval):
    while time_interval > 0:
        client.client_send_heartbeat()
        time.sleep(int(LONGEST_SURVIVAL_TIME/2))
        time_interval -= int(LONGEST_SURVIVAL_TIME/2)