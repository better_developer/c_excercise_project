import unittest
from subprocess import Popen
from subprocess import DEVNULL
from constants import *
from client_python import *

client = Client()
client.client_init(LOCAL_HOST, server_port)


def setUpModule():
    print("\ntest begin")


def tearDownModule():
    client.client_deinit()


class ClientTest(unittest.TestCase):
    def __init(self, method_name):
        unittest.TestCase.__init__(self, method_name)

    
    def setUp(self):
        print("start...")
    

    def tearDown(self):
        print("------------------testcase end------------------\n")


    def test_server_bind_fail(self):
        # Given
        # When
        server = Popen("../bin/server_main", stdout=DEVNULL)
        server.wait()
        # Then


if __name__ == '__main__':
    unittest.main(verbosity=2)
