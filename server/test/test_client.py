import unittest
import signal
from subprocess import Popen
from subprocess import DEVNULL
from constants import *
from client_python import *
from common_func import *
from user import *

Server = Popen("../bin/server_main", stdout=DEVNULL)


def setUpModule():
    print("\ntest begin")


def tearDownModule():
    # Server.kill()
    Server.send_signal(signal.SIGINT)


class ClientTest(unittest.TestCase):
    def __init(self, method_name):
        unittest.TestCase.__init__(self, method_name)


    def setUp(self):
        create_clients(MAX_USER_NUM+1)
    

    def tearDown(self):
        release_all_clients(MAX_USER_NUM+1)
        print("------------------testcase end------------------\n")
    

    def test01_err_client_id(self):
        # Given
        client_id = 0

        # When
        Clients[0].client_send_conn_request(client_id)

        # Then


    def test02_existed_id_case_of_client_conn(self):
        # Given
        expected_id = 1
        expected_recv_conn_responses = [pack_client_conn_response_with_success(expected_id), pack_client_conn_reponse_with_existed_id(expected_id)]

        # When
        recv_conn_responses = []
        for i in range(2):
            Clients[i].client_send_conn_request(expected_id)
            recv_conn_responses.append(Clients[i].client_recv_conn_response())

        # Then
        for i in range(2):
            self.assertEqual(recv_conn_responses[i], expected_recv_conn_responses[i])


    def test03_full_list_case_of_client_conn(self):
        # Given
        expected_recv_conn_responses = create_expected_client_conn_response_of_full_list_case(MAX_USER_NUM+1)
                
        # When
        recv_conn_responses = []
        for i in range(MAX_USER_NUM+1):
            Clients[i].client_send_conn_request(i+1)
            recv_conn_responses.append(Clients[i].client_recv_conn_response())

        # Then
        for i in range(MAX_USER_NUM):
            self.assertEqual(recv_conn_responses[i], expected_recv_conn_responses[i])
    

    def test04_successful_case_of_client_conn(self):
        # Given
        expected_id = 1
        expected_recv_data = pack_client_conn_response_with_success(expected_id)

        # When
        Clients[0].client_send_conn_request(expected_id)
        recv_data = Clients[0].client_recv_conn_response()

        # Then
        self.assertEqual(recv_data, expected_recv_data)
    

    def test05_process_chat_msg_from_client_to_client_unsuccessfully(self):
        # Given
        src_id = 1
        dest_id = 2
        msg = "hello"
        expected_recv_data = pack_chat_msg_forward_unsuccessfully_response(src_id)
        
        # When
        Clients[0].client_send_conn_request(src_id)
        Clients[0].client_send_chat_msg(src_id, dest_id, len(msg), msg)
        recv_data = Clients[0].client_recv_chat_msg_response()
        
        # Then
        self.assertEqual(recv_data, expected_recv_data)


    def test06_process_client_chat_msg_to_client_successfully(self):
        # Given
        src_id = 1
        dest_id = 2
        msg = "hello"
        expected_recv_chat_msg = pack_chat_msg(src_id, dest_id, len(msg), msg)
        expected_recv_chat_msg_response = pack_chat_msg_forward_successfully_response(src_id)
        
        # When
        Clients[0].client_send_conn_request(src_id)
        Clients[1].client_send_conn_request(dest_id)
        Clients[0].client_send_chat_msg(src_id, dest_id, len(msg), msg)
        recv_chat_msg_response = Clients[0].client_recv_chat_msg_response()
        recv_chat_msg = Clients[1].client_recv_chat_msg()
        
        # Then
        self.assertEqual(recv_chat_msg_response, expected_recv_chat_msg_response)
        self.assertEqual(recv_chat_msg, expected_recv_chat_msg)
    

    def test07_client_send_heartbeat_interrupted(self):
        # Given
        src_id = 1
        dest_id = 2
        msg = "hello"
        expected_recv_chat_msg_response1 = pack_chat_msg_forward_successfully_response(src_id)
        expected_recv_chat_msg_response2 = pack_chat_msg_forward_unsuccessfully_response(src_id)

        # When
        Clients[0].client_send_conn_request(src_id)
        Clients[1].client_send_conn_request(dest_id)
        Clients[0].client_send_heartbeat()
        Clients[1].client_send_heartbeat()
        Clients[0].client_send_chat_msg(src_id, dest_id, len(msg), msg)
        recv_chat_msg_response1 = Clients[0].client_recv_chat_msg_response()
        client_send_heartbeat_continuously(Clients[0], LONGEST_SURVIVAL_TIME+1)
        Clients[0].client_send_chat_msg(src_id, dest_id, len(msg), msg)
        recv_chat_msg_response2 = Clients[0].client_recv_chat_msg_response()

        # Then
        self.assertEqual(recv_chat_msg_response1, expected_recv_chat_msg_response1)
        self.assertEqual(recv_chat_msg_response2, expected_recv_chat_msg_response2)


    def test08_client_send_heartbeat_normally(self):
        # Given
        src_id = 1
        dest_id = 2
        msg = "hello"
        expected_recv_chat_msg_response = pack_chat_msg_forward_successfully_response(src_id)

        # When
        Clients[0].client_send_conn_request(src_id)
        Clients[1].client_send_conn_request(dest_id)
        Clients[0].client_send_heartbeat()
        Clients[1].client_send_heartbeat()
        Clients[0].client_send_chat_msg(src_id, dest_id, len(msg), msg)
        recv_chat_msg_response = Clients[0].client_recv_chat_msg_response()
        
        # Then
        self.assertEqual(recv_chat_msg_response, expected_recv_chat_msg_response)


    def test09_client_release_request(self):
        # Given
        src_id = 1
        dest_id = 2
        msg = "hello"
        expected_recv_chat_msg = pack_chat_msg(src_id, dest_id, len(msg), msg)
        expected_recv_chat_response1 = pack_chat_msg_forward_successfully_response(src_id)
        expected_recv_chat_response2 = pack_chat_msg_forward_unsuccessfully_response(src_id)

        # When
        Clients[0].client_send_conn_request(src_id)
        Clients[1].client_send_conn_request(dest_id)

        Clients[0].client_send_chat_msg(src_id, dest_id, len(msg), msg)
        recv_chat_msg = Clients[1].client_recv_chat_msg()
        recv_chat_response1 = Clients[0].client_recv_chat_msg_response()
        Clients[1].client_send_release_request()
        Clients[0].client_send_chat_msg(src_id, dest_id, len(msg), msg)
        recv_chat_reponse2 = Clients[0].client_recv_chat_msg_response()

        # Then
        self.assertEqual(recv_chat_msg, expected_recv_chat_msg)
        self.assertEqual(recv_chat_response1, expected_recv_chat_response1)
        self.assertEqual(recv_chat_reponse2, expected_recv_chat_response2)


    def test10_err_api_id(self):
        # Given
        err_api_id = 20
        client_id = 1

        # When
        Clients[0].client_send_conn_request(client_id)
        Clients[0].client_send_err_api(err_api_id)

        # Then


if __name__ == '__main__':
    unittest.main(verbosity=2)
