from socket import SOCK_RAW
import struct
from constants import *


def unpack_header(data):
    api_id = struct.unpack("!B", data[:1])[0]
    return api_id


def pack_chat_msg(src_id, dest_id, msg_len, msg):
    data = struct.pack("!BHHH%ds" % msg_len, CLIENT_SEND_CHAT_MESSAGE_API_ID, src_id, dest_id, msg_len, bytes(msg.encode('utf-8')))
    return data


def unpack_chat_msg(recv_data):
    api_id, src_id, dest_id, msg_len = struct.unpack("!BHHH", recv_data[:7])
    msg = struct.unpack("%ds" % msg_len, recv_data[7:])[0].decode('utf-8')
    return api_id, src_id, dest_id, msg_len, msg
