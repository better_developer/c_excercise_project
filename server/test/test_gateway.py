import unittest
import signal
import time
from subprocess import Popen
from subprocess import DEVNULL
from constants import *
from gateway_python import *
from client_python import *
from common_func import *
from user import *

Server = Popen("../bin/server_main", stdout=DEVNULL)


def setUpModule():
    print("test begin")


def tearDownModule():
    Server.send_signal(signal.SIGINT)


class GatewayTest(unittest.TestCase):
    def __init(self, method_name):
        unittest.TestCase.__init__(self, method_name)


    def setUp(self):
        create_gateways(MAX_USER_NUM+1)
        create_clients(MAX_USER_NUM)


    def tearDown(self):
        release_all_gateways(MAX_USER_NUM+1)
        release_all_clients(MAX_USER_NUM)
        print("------------------testcase end------------------\n")


    def test01_err_gateway_id(self):
        # Given
        gateway_id = 0

        # When
        Gateways[0].gateway_send_conn_request(gateway_id)
        time.sleep(3)

        # Then


    def test02_existed_id_case_of_gateway_conn(self):
        # Given
        expected_id = 1
        ecpected_recv_conn_response = [pack_gateway_conn_response_with_success(expected_id), pack_gateway_conn_reponse_with_existed_id(expected_id)]

        # When
        recv_conn_response = []
        for i in range(2):
            Gateways[i].gateway_send_conn_request(expected_id)
            recv_conn_response.append(Gateways[i].gateway_recv_conn_response())
        
        # Then
        for i in range(2):
            self.assertEqual(recv_conn_response[i], ecpected_recv_conn_response[i])


    def test03_full_list_case_of_gateway_conn(self):
        # Given
        expected_recv_conn_responses = create_expected_gateway_conn_reponse_of_full_list_case(MAX_USER_NUM+1)

        # When
        recv_conn_responses = []
        for i in range(MAX_USER_NUM+1):
            Gateways[i].gateway_send_conn_request(i+1)
            recv_conn_responses.append(Gateways[i].gateway_recv_conn_response())
        
        # Then
        for i in range(MAX_USER_NUM):
            self.assertEqual(recv_conn_responses[i], expected_recv_conn_responses[i])
    

    def test04_successful_case_of_gateway_conn(self):
        # Given
        expected_id = 1
        expected_recv_data = pack_gateway_conn_response_with_success(expected_id)

        # When
        Gateways[0].gateway_send_conn_request(expected_id)
        recv_data = Gateways[0].gateway_recv_conn_response()

        # Then
        self.assertEqual(recv_data, expected_recv_data)
    

    def test05_err_client_id_from_gateway(self):
        # Given
        client_id = 0

        # When
        Gateways[0].gateway_send_conn_request(1)
        Gateways[0].gateway_send_client_conn_notice(client_id)

        # Then


    def test06_process_client_conn_notice_with_existed_id(self):
        # Given
        expected_client_id = 1
        expected_recv_client_conn_responses = []
        expected_recv_client_conn_responses = [pack_client_conn_response_with_success(expected_client_id), pack_client_conn_reponse_with_existed_id(expected_client_id)]
        
        # When
        Gateways[0].gateway_send_conn_request(1)
        recv_client_conn_responses = []
        for i in range(2):
            Gateways[0].gateway_send_client_conn_notice(expected_client_id)
            recv_client_conn_responses.append(Gateways[0].gateway_recv_client_conn_response())
        Gateways[0].gateway_send_client_release_notice(expected_client_id)

        # Then
        for i in range(2):
            self.assertEqual(recv_client_conn_responses[i], expected_recv_client_conn_responses[i])    


    def test07_process_client_conn_notice_with_full_list(self):
        # Given
        expected_recv_client_conn_reponses = create_expected_conn_response_of_client_conn_notice_with_full_list(MAX_USER_NUM+1)

        # When
        Gateways[0].gateway_send_conn_request(1)
        recv_client_conn_responses = []
        for i in range(MAX_USER_NUM+1):
            Gateways[0].gateway_send_client_conn_notice(i+1)
            recv_client_conn_responses.append(Gateways[0].gateway_recv_client_conn_response())
        for i in range(MAX_USER_NUM):
            Gateways[0].gateway_send_client_release_notice(i+1)
        
        # Then
        for i in range(MAX_USER_NUM+1):
            self.assertEqual(recv_client_conn_responses[i], expected_recv_client_conn_reponses[i])


    def test08_process_client_conn_notice_with_success(self):
        # Given
        expected_client_id = 1
        expected_recv_client_conn_response = pack_client_conn_response_with_success(expected_client_id)

        # When
        Gateways[0].gateway_send_conn_request(1)
        Gateways[0].gateway_send_client_conn_notice(expected_client_id)
        recv_client_conn_response = Gateways[0].gateway_recv_client_conn_response()
        Gateways[0].gateway_send_client_release_notice(expected_client_id)

        # Then
        self.assertEqual(recv_client_conn_response, expected_recv_client_conn_response)


    def test09_gateway_heartbeat_interrupted(self):
        # Given
        gateway_id = 1
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        expected_recv_chat_msg = pack_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        expected_recv_chat_response1 = pack_chat_msg_forward_successfully_response(src_client_id)
        expected_recv_chat_response2 = pack_chat_msg_forward_unsuccessfully_response(src_client_id)

        # When
        Gateways[0].gateway_send_conn_request(gateway_id)
        Gateways[0].gateway_send_heartbeat()
        time.sleep(3)

        Gateways[0].gateway_send_client_conn_notice(dest_client_id)
        Clients[0].client_send_conn_request(src_client_id)
        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_msg = Gateways[0].gateway_recv_chat_msg_from_server()
        recv_chat_response1 = Clients[0].client_recv_chat_msg_response()
        
        time.sleep(3)
        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_response2 = Clients[0].client_recv_chat_msg_response()

        # Then
        self.assertEqual(recv_chat_msg, expected_recv_chat_msg)
        self.assertEqual(recv_chat_response1, expected_recv_chat_response1)
        self.assertEqual(recv_chat_response2, expected_recv_chat_response2)


    def test010_gateway_send_heartbeat_normally(self):
        # Given
        gateway_id = 1
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        expected_recv_chat_msg1 = pack_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        expected_recv_chat_msg2 = pack_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        expected_recv_chat_response1 = pack_chat_msg_forward_successfully_response(src_client_id)
        expected_recv_chat_response2 = pack_chat_msg_forward_successfully_response(src_client_id)

        # When
        Gateways[0].gateway_send_conn_request(gateway_id)
        Gateways[0].gateway_send_heartbeat()
        time.sleep(4)

        Gateways[0].gateway_send_client_conn_notice(dest_client_id)
        Clients[0].client_send_conn_request(src_client_id)
        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_msg1 = Gateways[0].gateway_recv_chat_msg_from_server()
        recv_chat_response1 = Clients[0].client_recv_chat_msg_response()
        Gateways[0].gateway_send_heartbeat()
        time.sleep(3)

        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_msg2 = Gateways[0].gateway_recv_chat_msg_from_server()
        recv_chat_response2 = Clients[0].client_recv_chat_msg_response()

        # Then
        self.assertEqual(recv_chat_msg1, expected_recv_chat_msg1)
        self.assertEqual(recv_chat_msg2, expected_recv_chat_msg2)
        self.assertEqual(recv_chat_response1, expected_recv_chat_response1)
        self.assertEqual(recv_chat_response2, expected_recv_chat_response2)
    

    def test11_process_client_release_notice(self):
        # Given
        gateway_id = 1
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        expected_recv_chat_msg = pack_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        expected_recv_chat_response1 = pack_chat_msg_forward_successfully_response(src_client_id)
        expected_recv_chat_response2 = pack_chat_msg_forward_unsuccessfully_response(src_client_id)

        # When
        Gateways[0].gateway_send_conn_request(gateway_id)

        Gateways[0].gateway_send_client_conn_notice(dest_client_id)
        Clients[0].client_send_conn_request(src_client_id)
        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_msg = Gateways[0].gateway_recv_chat_msg_from_server()
        recv_chat_response1 = Clients[0].client_recv_chat_msg_response()
        
        Gateways[0].gateway_send_client_release_notice(dest_client_id)
        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_response2 = Clients[0].client_recv_chat_msg_response()

        # Then
        self.assertEqual(recv_chat_msg, expected_recv_chat_msg)
        self.assertEqual(recv_chat_response1, expected_recv_chat_response1)
        self.assertEqual(recv_chat_response2, expected_recv_chat_response2)


    def test12_gateway_release_request(self):
        # Given
        gateway_id = 1
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        expected_recv_chat_msg = pack_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        expected_recv_chat_response1 = pack_chat_msg_forward_successfully_response(src_client_id)
        expected_recv_chat_response2 = pack_chat_msg_forward_unsuccessfully_response(src_client_id)

        # When
        Gateways[0].gateway_send_conn_request(gateway_id)

        Gateways[0].gateway_send_client_conn_notice(dest_client_id)
        Clients[0].client_send_conn_request(src_client_id)
        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_msg = Gateways[0].gateway_recv_chat_msg_from_server()
        recv_chat_response1 = Clients[0].client_recv_chat_msg_response()
        
        Gateways[0].gateway_send_release_request()
        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_response2 = Clients[0].client_recv_chat_msg_response()

        # Then
        self.assertEqual(recv_chat_msg, expected_recv_chat_msg)
        self.assertEqual(recv_chat_response1, expected_recv_chat_response1)
        self.assertEqual(recv_chat_response2, expected_recv_chat_response2)

        
    def test13_process_chat_msg_from_gateway_to_client_unsuccessfully(self):
        # Given
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        gateway_id = 1
        expected_recv_chat_response = pack_chat_msg_forward_unsuccessfully_response(src_client_id)

        # When
        Gateways[0].gateway_send_conn_request(gateway_id)
        Gateways[0].gateway_send_client_conn_notice(src_client_id)
        Gateways[0].gateway_forward_chat_msg_to_server(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_response = Gateways[0].gateway_recv_chat_msg_response_from_server()
        Gateways[0].gateway_send_client_release_notice(src_client_id)
        time.sleep(3)

        # Then
        self.assertEqual(recv_chat_response, expected_recv_chat_response)


    def test14_process_chat_msg_from_gateway_to_client_successfully(self):
        # Given
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        gateway_id = 1
        expected_recv_chat_response = pack_chat_msg_forward_successfully_response(src_client_id)
        expected_recv_chat_msg = pack_chat_msg(src_client_id, dest_client_id, len(msg), msg)

        # When
        Gateways[0].gateway_send_conn_request(gateway_id)
        Gateways[0].gateway_send_client_conn_notice(src_client_id)
        Clients[0].client_send_conn_request(dest_client_id)

        Gateways[0].gateway_forward_chat_msg_to_server(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_response = Gateways[0].gateway_recv_chat_msg_response_from_server()
        recv_chat_msg = Clients[0].client_recv_chat_msg()
        Gateways[0].gateway_send_client_release_notice(src_client_id)
        time.sleep(3)

        # Then
        self.assertEqual(recv_chat_response, expected_recv_chat_response)
        self.assertEqual(recv_chat_msg, expected_recv_chat_msg)
    

    def test15_process_chat_msg_from_client_to_gateway_unsuccessfully(self):
        # Given
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        expected_recv_chat_response = pack_chat_msg_forward_unsuccessfully_response(src_client_id)

        # When
        Clients[0].client_send_conn_request(src_client_id)
        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_response = Clients[0].client_recv_chat_msg_response()
        time.sleep(3)

        # Then
        self.assertEqual(recv_chat_response, expected_recv_chat_response)


    def test16_process_chat_msg_from_client_to_gateway_successfully(self):
        # Given
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        gateway_id = 1
        expected_recv_chat_response = pack_chat_msg_forward_successfully_response(src_client_id)
        expected_recv_chat_msg = pack_chat_msg(src_client_id, dest_client_id, len(msg), msg)

        # When
        Gateways[0].gateway_send_conn_request(gateway_id)
        Gateways[0].gateway_send_client_conn_notice(dest_client_id)
        Clients[0].client_send_conn_request(src_client_id)
        Clients[0].client_send_chat_msg(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_response = Clients[0].client_recv_chat_msg_response()
        recv_chat_msg = Gateways[0].gateway_recv_chat_msg_from_server()
        Gateways[0].gateway_send_client_release_notice(dest_client_id)
        time.sleep(3)

        # Then
        self.assertEqual(recv_chat_response, expected_recv_chat_response)
        self.assertEqual(recv_chat_msg, expected_recv_chat_msg)


    def test17_process_chat_msg_from_gateway_to_gateway_unsuccessfully(self):
        # Given
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        gateway_id = 1
        expected_recv_chat_response = pack_chat_msg_forward_unsuccessfully_response(src_client_id)

        # When
        Gateways[0].gateway_send_conn_request(gateway_id)
        Gateways[0].gateway_send_client_conn_notice(src_client_id)
        Gateways[0].gateway_forward_chat_msg_to_server(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_response = Gateways[0].gateway_recv_chat_msg_response_from_server()
        Gateways[0].gateway_send_client_release_notice(src_client_id)
        time.sleep(3)

        # Then
        self.assertEqual(recv_chat_response, expected_recv_chat_response)


    def test18_process_chat_msg_from_gateway_to_gateway_successfully(self):
        # Given
        src_client_id = 1
        dest_client_id = 2
        msg = "hello"
        gateway1_id = 1
        gateway2_id = 2
        expected_recv_chat_response = pack_chat_msg_forward_successfully_response(src_client_id)
        expected_recv_chat_msg = pack_chat_msg(src_client_id, dest_client_id, len(msg), msg)

        # When
        Gateways[0].gateway_send_conn_request(gateway1_id)
        Gateways[1].gateway_send_conn_request(gateway2_id)
        Gateways[0].gateway_send_client_conn_notice(src_client_id)
        Gateways[1].gateway_send_client_conn_notice(dest_client_id)
        Gateways[0].gateway_forward_chat_msg_to_server(src_client_id, dest_client_id, len(msg), msg)
        recv_chat_msg = Gateways[1].gateway_recv_chat_msg_from_server()
        recv_chat_response = Gateways[0].gateway_recv_chat_msg_response_from_server()
        Gateways[0].gateway_send_client_release_notice(src_client_id)
        Gateways[1].gateway_send_client_release_notice(dest_client_id)
        time.sleep(3)

        # Then
        self.assertEqual(recv_chat_response, expected_recv_chat_response)
        self.assertEqual(recv_chat_msg, expected_recv_chat_msg)
    

    def test19_send_heartbeat_error(self):
        # Given
        client_id = 1
        gateway_id = 1
        err_client_id = 20
        err_gateway_id = 20

        # When
        Clients[0].client_send_conn_request(client_id)
        Gateways[0].gateway_send_conn_request(gateway_id)
        Clients[0].client_send_heartbeat_err(err_client_id)
        Gateways[0].gateway_send_heartbeat_err(err_gateway_id)

        # Then

    
    def test20_remove_err(self):
        # Given
        client_id = 1
        gateway_id = 1
        err_client_id = 20
        err_gateway_id = 20

        # When
        Clients[0].client_send_conn_request(client_id)
        Gateways[0].gateway_send_conn_request(gateway_id)
        Clients[0].client_send_err_release_request(err_client_id)
        Gateways[0].gateway_send_err_release_request(err_gateway_id)

        # Then


if __name__ == '__main__':
    unittest.main(verbosity=2)
