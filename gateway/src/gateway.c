#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <mcheck.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include "gateway.h"
#include "timer.h"
#include "message_format.h"
#include "user_list.h"
#include "api.h"

static bool gateway_status = GATEWAY_OFFLINE;
static UInt16 gateway_id = 0;
static struct socket_info socket_info;
static struct sockaddr_in  server_addr;

Timer global_connection_timer;

void Gateway_Init(int id, char *server_ip)
{
    gateway_id = id;
    Gateway_InitSocket(server_ip);
    UserList_Init();
    signal(SIGINT, Gateway_Release);
    Gateway_InitConnection();
}


void Gateway_SendToClient(void *send_buf, size_t buf_size, struct sockaddr_in *client_addr)
{
    if (sendto(socket_info.socket_fd, send_buf, buf_size, 0, (struct sockaddr *)client_addr, sizeof(struct sockaddr)) < 0) {
        perror("failed to send meg to client");
    }
}


void Gateway_SendToServer(void *send_buf, size_t buf_size)
{
    if (sendto(socket_info.socket_fd, send_buf, buf_size, 0, (struct sockaddr *)&socket_info.serv_addr, sizeof(struct sockaddr)) < 0) {
        perror("failed to send msg to server");
    }
}


UInt16 Gateway_GetId(void)
{
    return gateway_id;
}


void Gateway_SetStatus(void)
{
    gateway_status = GATEWAY_ONLINE;
}


bool Gateway_GetStatus(void)
{
    return gateway_status;
}


void Gateway_InitSocket(char *server_ip)
{
    int socket_fd;
    struct sockaddr_in gateway_addr;

    socket_fd = socket(AF_INET, SOCK_DGRAM, 0);
    printf("init:%d\n", socket_fd);
    if (socket_fd < 0) {
        perror("failed to create socket");
        exit(-1);
    }

    gateway_addr.sin_family = AF_INET;
    gateway_addr.sin_port = htons(GATEWAY_PORT_NUMBER);
    gateway_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(socket_fd, (struct sockaddr *)&gateway_addr, sizeof(gateway_addr)) < 0) {
        perror("failed to bind");
        exit(-1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT_NUMBER);
    server_addr.sin_addr.s_addr = inet_addr(server_ip);

    socket_info.socket_fd = socket_fd;
    socket_info.serv_addr = server_addr;
}


struct socket_info *Gateway_GetSocketInfo(void)
{
    return &socket_info;
}


static void Gateway_SendConnectRequest(void)
{
    struct common_info connect_request = {
        .header.api_id = GATEWAY_CONNECTION_REQUEST_API_ID,
        .body.client_id_or_gateway_id = htons(gateway_id),
    };

    printf("sending connect request\n");

    Gateway_SendToServer(&connect_request, sizeof(connect_request));
}


void Gateway_InitConnection(void)
{
    Gateway_SendConnectRequest();
    Timer_CreateTimer(&global_connection_timer, kCONNECT_REQUEST_INTERVAL, Gateway_SendConnectRequest);
    Timer_Start(&global_connection_timer);
}


void Gateway_SendHeartbeat(void)
{
    struct common_info heartbeat_msg = {
        .header.api_id = GATEWAY_HEARTBEAT_API_ID,
        .body.client_id_or_gateway_id = htons(gateway_id),
    };

    Gateway_SendToServer( &heartbeat_msg, sizeof(heartbeat_msg));
}


void Gateway_Release(int signo)
{
    struct client *p = UserList_GetHead()->next;
    struct common_message_header release_notice = {
        .api_id = GATEWAY_RELEASE_NOTICE_API_ID,
    };
    struct common_info release_request = {
        .header.api_id = GATEWAY_RELEASE_REQUEST_API_ID,
        .body.client_id_or_gateway_id = htons(gateway_id),
    };

    Gateway_SendToServer(&release_request, sizeof(release_request));

    while (p != NULL) {
        Gateway_SendToClient(&release_notice, sizeof(release_notice), &p->client_addr);
        p = p->next;
    }

    Gateway_Deinit();
    exit(kGATEWAY_RELEASE);
}


void Gateway_Deinit(void) 
{
    close(socket_info.socket_fd);
    UserList_Deinit();
    muntrace();
}