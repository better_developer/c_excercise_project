#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>
#include <mcheck.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <pthread.h>
#include "message_format.h"
#include "user_list.h"
#include "api.h"
#include "gateway.h"

static int count = 0;
static struct client *head = NULL;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void UserList_Init(void)
{
    printf("list created\n");
    head = calloc(1, sizeof(struct client));
    head->next = NULL;
}


struct client *UserList_GetHead(void)
{
    return head;
}


bool UserList_Find(UInt16 user_id, struct client_info *info)
{
    struct client *p = head->next;

    while (p != NULL) {
        if (p->user_id == user_id) {
            memcpy(&info->client_addr, &p->client_addr, sizeof(struct sockaddr_in));
            info->client_status = p->client_status;
            info->heartbeat_time = p->heartbeat_time;
            return CLIENT_EXIST;
        }
        p = p->next;
    }

    return CLIENT_NOT_EXIST;
}


bool UserList_ClientIdExist(UInt16 user_id)
{
    struct client *p = head->next;

    while (p != NULL) {
        if (p->user_id == user_id) {
            return CLIENT_EXIST;
        }
        p = p->next;
    }

    printf("client NOT exist\n");
    return CLIENT_NOT_EXIST;
}


void UserList_SetClientStatus(UInt16 user_id)
{
    struct client *p = head->next;

    while (p != NULL) {
        if (p->user_id == user_id) {
            p->client_status = CLIENT_ONLINE;
            return;
        }
        p = p->next;
    }
}


void UserList_Add(UInt16 user_id, struct sockaddr_in *client_addr)
{
    pthread_mutex_lock(&mutex);
    struct client *new_client = calloc(1, sizeof(struct client));

    printf("running client add\n");

    new_client->user_id = user_id;
    new_client->client_status = CLIENT_OFFLINE;
    memcpy(&new_client->client_addr, client_addr, sizeof(struct sockaddr));
    clock_gettime(CLOCK_REALTIME, &new_client->heartbeat_time);
    
    new_client->next = head->next;
    head->next = new_client;
    pthread_mutex_unlock(&mutex);

    count++;
}


void UserList_Remove(UInt16 user_id)
{
    struct client *p = head;
    struct client *q = head->next;
    
    while (q != NULL) {
        if (q->user_id == user_id) {
            pthread_mutex_lock(&mutex);
            p->next = q->next;
            q->next = NULL;
            free(q);
            count--;
            pthread_mutex_unlock(&mutex);
            return;
        }
        p = q;
        q = q->next;
    }
}


void UserList_PrintOnlineUsers(void)
{
    struct client *p = head->next;

    printf("running printOnlineUsers\n");
    printf("clientID\t IP\t\n");
    
    while (p != NULL) {
        if (p->client_status == CLIENT_ONLINE) {
            printf("  %d\t\t %d\t\n", p->user_id, p->client_addr.sin_addr.s_addr);
        }
        p = p->next;
    }
}


void UserList_CheckHeartbeatTime(void)
{
    struct client *p = head->next;
    struct timespec current;
    int duration = 0;
    struct client_release_notice_from_gateway release_msg;
    UInt16 user_id = 0;

    printf("running onlineCheck\n");
    clock_gettime(CLOCK_REALTIME, &current);
    while (p != NULL) {
        duration = current.tv_sec - p->heartbeat_time.tv_sec;
        if (duration > kTIMEOUT_INTERVAL) {
            release_msg.header.api_id = CLIENT_RELEASE_NOTICE_FROM_GATEWAY_API_ID;
            release_msg.body.client_id = htons(p->user_id);
            Gateway_SendToServer(&release_msg, sizeof(release_msg));

            user_id = p->user_id;
            p = p->next;
            UserList_Remove(user_id);
            printf("client release without heartbeat\n");
            continue;
        }
        p = p->next;
    }
}


void UserList_Deinit(void)
{
    struct client *p = head->next;

    while (p != NULL) {
        p = p->next;
        free(head->next);
        head->next = p;
    }
    printf("running userlist deinit\n");
    free(head);
}


bool UserList_IsFull(void)
{
    if (count >= kMAX_CLIENT_NUM) {
        return true;
    }

    return false;
}


void UserList_UpdateHeartbeat(UInt16 user_id)
{
    struct client *p = head->next;
    
    while (p != NULL) {
        if (p->user_id == user_id) {
            pthread_mutex_lock(&mutex);
            clock_gettime(CLOCK_REALTIME, &p->heartbeat_time);
            pthread_mutex_unlock(&mutex);
            return;
        }
        p = p->next;
    }
}
