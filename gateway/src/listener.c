#include <stdio.h>
#include <string.h>
#include <mcheck.h>
#include "message.h"
#include "message_format.h"
#include "listener.h"
#include "api.h"
#include "gateway.h"

void *Listener_RecvMsg(void *arg)
{
    char recv_buf[kMAX_MESSAGE_LEN] = {0};
    struct sockaddr_in recv_addr;
    socklen_t recv_addrlen = sizeof(recv_addr);
    struct socket_info *socket_info = Gateway_GetSocketInfo();
    struct common_message_header *recv_header;
    UInt8 api_id;

    printf("running listenning\n");
    while (1) {
        memset(recv_buf, 0, kMAX_MESSAGE_LEN);
        if (recvfrom(socket_info->socket_fd, recv_buf, sizeof(recv_buf), 0, (struct sockaddr *)&recv_addr, &recv_addrlen) < 0) {
            perror("failed to recv");
        }
        recv_header = (struct common_message_header *)recv_buf;
        api_id = recv_header->api_id;
        
        if (Gateway_GetStatus() != GATEWAY_ONLINE) {
            if (api_id == GATEWAY_CONNECTION_RESPONSE_API_ID) {
                Message_ProcessGatewayLoginResponse(recv_buf);
            }
            continue;
        }
        
        switch(api_id)
        {
        case CLIENT_CONNECTION_REQUEST_API_ID:
            Message_ProcessClientLogin(recv_buf, &recv_addr);
            break;

        case CLIENT_CONNECTION_RESPONSE_API_ID:
            Message_ProcessClientLoginResponse(recv_buf);
            break;

        case CLIENT_RELEASE_REQUEST_API_ID:
            Message_ProcessClientReleaseRequest(recv_buf);
            break;

        case CLIENT_HEARTBEAT_API_ID:
            Message_ProcessClientHeartbeat(recv_buf);
            break;

        case CLIENT_SEND_CHAT_MESSAGE_API_ID:
            Message_ProcessClientChat(recv_buf);
            break;

        case CHAT_MESSAGE_RESPONSE_API_ID:
            Message_ProcessClientChatResponse(recv_buf);
            break;

        default:
            perror("Invalid api_id");
            break;
        }
    }
}
