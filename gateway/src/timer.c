#include <string.h>
#include <mcheck.h>
#include "timer.h"


static void Timer_Setup(Timer *self)
{
    memset(&self->ent, 0x00, sizeof(struct sigevent));
    self->ent.sigev_notify = SIGEV_THREAD;
    self->ent.sigev_notify_function = self->timeout_handler;

    timer_create(CLOCK_REALTIME, &self->ent, &self->timer_id);
}


static void Timer_Run(Timer *self)
{
    self->value.it_value.tv_sec = self->seconds;
    self->value.it_value.tv_nsec = 0;
    self->value.it_interval.tv_sec = self->seconds;
    self->value.it_interval.tv_nsec = 0;

    timer_settime(self->timer_id, 0, &self->value, NULL);
}


void Timer_CreateTimer(Timer *self, UInt32 seconds, void *timeout_handler)
{
    self->seconds = seconds;
    self->timeout_handler = timeout_handler;
    Timer_Setup(self);
}


void Timer_Start(Timer *self)
{
    Timer_Run(self);
}


void Timer_DeleteTimer(Timer *self)
{
    timer_delete(self->timer_id);
}
