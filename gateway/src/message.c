#include <assert.h>
#include <stdlib.h>
#include <mcheck.h>
#include "message_format.h"
#include "type.h"
#include "user_list.h"
#include "message.h"
#include "api.h"
#include "timer.h"
#include "gateway.h"
#include "error_code.h"
#include "response_status.h"

extern Timer global_connection_timer;

static void respondClientConnectErr(UInt8 error_code, UInt16 user_id, struct sockaddr_in *client_addr)
{
    struct common_response response_msg;

    memset(&response_msg, 0, sizeof(response_msg));
    response_msg.header.api_id = CLIENT_CONNECTION_RESPONSE_API_ID;
    response_msg.body.client_id_or_gateway_id = htons(user_id);
    response_msg.body.status = kRESPONSE_STATUS_ERROR;
    response_msg.body.error_code = error_code;

    Gateway_SendToClient(&response_msg, sizeof(response_msg), client_addr);
}


static void forwardClientConnectRequest(UInt16 user_id)
{
    struct client_connect_request_forwarding forward_msg;

    printf("running forwardClientConnectRequest\n");

    memset(&forward_msg, 0, sizeof(forward_msg));
    forward_msg.header.api_id = CONNECTION_REQUEST_FORWARDING_API_ID;
    forward_msg.body.client_id = htons(user_id);
    forward_msg.body.gateway_id = htons(Gateway_GetId());
    Gateway_SendToServer(&forward_msg, sizeof(forward_msg));
}


void Message_ProcessClientLogin(char *buf, struct sockaddr_in *client_addr)
{
    struct common_info *connect_request = (struct common_info *)buf;
    UInt16 user_id;
    struct client_info client = {0};
    bool client_exist;

    assert(buf);
    assert(client_addr);

    user_id = ntohs(connect_request->body.client_id_or_gateway_id);

    printf("running clientLogin\n");
    if (UserList_IsFull()) {
        respondClientConnectErr(CONNECTED_ERR_OF_LIST_FULL, user_id, client_addr);
        return;
    }

    client_exist = UserList_Find(user_id, &client) ;
    if (!client_exist) {
        UserList_Add(user_id, client_addr);
        forwardClientConnectRequest(user_id);
        return;
    }

    if (client.client_status == CLIENT_ONLINE) {
        printf("id existed!\n");
        respondClientConnectErr(CONNECTED_ERR_OF_ID_EXISTED, user_id, client_addr);
        return;
    }
    forwardClientConnectRequest(user_id);
}


void Message_ProcessClientLoginResponse(char *buf)
{
    printf("running ProcessClientLoginResponse\n");
    struct common_response *recv_response = (struct common_response *)buf;
    struct client_info client;

    assert(buf);

    UInt16 user_id = ntohs(recv_response->body.client_id_or_gateway_id);
    bool client_exist = UserList_Find(user_id, &client);

    if (!client_exist) {
        return;
    }
    Gateway_SendToClient(recv_response, sizeof(*recv_response), &client.client_addr);

    if (recv_response->body.status == kRESPONSE_STATUS_ERROR) {
        UserList_Remove(user_id);
        return;
    }
    UserList_SetClientStatus(user_id);
}


void Message_ProcessGatewayLoginResponse(char *buf)
{
    printf("running processGatewayLoginResponse\n");
    struct common_response *recv_response = (struct common_response *)buf;

    assert(buf);

    if (recv_response->body.status == kRESPONSE_STATUS_OK) {
        Timer_DeleteTimer(&global_connection_timer);
        Gateway_SetStatus();
        return;
    }

    if (recv_response->body.error_code == CONNECTED_ERR_OF_ID_EXISTED) {
        printf("Existed GatewayID!\n");
        Gateway_Deinit();
        exit(kGATEWAY_ID_EXIST);
    }

    if (recv_response->body.error_code == CONNECTED_ERR_OF_LIST_FULL) {
        printf("Server list is full!\n");
        Gateway_Deinit();
        exit(kFULL_SERVER_LIST);
    }
}


void Message_ProcessClientReleaseRequest(char *buf)
{
    struct common_info *release_request = (struct common_info *)buf;

    assert(buf);
    
    UInt16 user_id = ntohs(release_request->body.client_id_or_gateway_id);

    printf("running Message_ProcessClientReleaseRequest\n");
    if (UserList_ClientIdExist(user_id) == CLIENT_EXIST) {
        printf("sending client release request to client\n");
        Gateway_SendToServer(release_request, sizeof(*release_request));
        UserList_Remove(user_id);
    }
}


void Message_ProcessClientHeartbeat(char *buf)
{
    struct common_info *recv_heartbeat = (struct common_info *)buf;
  
    assert(buf);
    
    UInt16 user_id = ntohs(recv_heartbeat->body.client_id_or_gateway_id);

    UserList_UpdateHeartbeat(user_id);
}


void Message_ProcessClientChat(char *buf)
{
    struct chat_message_from_client *chat_msg = (struct chat_message_from_client *)buf;
    struct client_info source_client, target_client;
    
    assert(buf);

    UInt16 source_client_id = ntohs(chat_msg->body.source_client_id);
    UInt16 target_client_id = ntohs(chat_msg->body.destination_client_id);

    bool client_exist = UserList_Find(target_client_id, &target_client);

    UserList_Find(source_client_id, &source_client);
    printf("running Message_ProcessClientChat\n");

    if (!client_exist) {
        printf("forward chat msg to server\n");
        Gateway_SendToServer(chat_msg, sizeof(*chat_msg)+ntohs(chat_msg->body.chat_msg_len));
        return;
    }
    if (target_client.client_status == CLIENT_ONLINE) {
        printf("forward chat msg to targetclient\n");
        Gateway_SendToClient(chat_msg, sizeof(*chat_msg)+ntohs(chat_msg->body.chat_msg_len), &target_client.client_addr);
    }
}


void Message_ProcessClientChatResponse(char *buf)
{
    struct common_response *response_msg = (struct common_response *)buf;
    struct client_info client;

    assert(buf);

    UInt16 client_id = ntohs(response_msg->body.client_id_or_gateway_id);
    bool client_exist = UserList_Find(client_id, &client);

    printf("running Message_ProcessClientChatResponse\n");
    if (client_exist && client.client_status == CLIENT_ONLINE) {
        printf("forward chat response to client\n");
        Gateway_SendToClient(response_msg, sizeof(*response_msg), &client.client_addr);
    }
}
