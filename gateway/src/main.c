#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <mcheck.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include "user_list.h"
#include "listener.h"
#include "gateway.h"

#define kSERVER_IP_VALID 1
#define kINPUT_SERVER_IP_INVALID 2
#define kINPUT_NUMBER_OF_ARGS_INVALID 1
#define kINPUT_GATEWAY_ID_INVALID 3


int main(int argc, char *argv[])
{
    mtrace();

    pthread_t thread_recv_id;
    struct in_addr addr;

    if (argc != 3) {
	    printf("please input gateway_id and server_ip\n");
        muntrace();
	    exit(kINPUT_NUMBER_OF_ARGS_INVALID);
    }

    if (inet_pton(AF_INET, argv[2], &addr) != kSERVER_IP_VALID) {
        printf("invalid server IP address\n");
        muntrace();
        exit(kINPUT_SERVER_IP_INVALID);
    }

    if (atoi(argv[1]) < 1) {
        printf("invalid gatewayId\n");
        muntrace();
        exit(kINPUT_GATEWAY_ID_INVALID);
    }

    Gateway_Init(atoi(argv[1]), argv[2]);
    pthread_create(&thread_recv_id, NULL, Listener_RecvMsg, NULL);

    while (1) {
        if (Gateway_GetStatus() == GATEWAY_OFFLINE) {
            sleep(1);
            continue;
        }
        Gateway_SendHeartbeat();
        UserList_CheckHeartbeatTime();
        UserList_PrintOnlineUsers();
        sleep(2);
    }

    muntrace();

    return 0;
}
