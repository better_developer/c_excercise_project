#pragma once
#include <stdbool.h>
#include "type.h"

struct socket_info {
    int socket_fd;
    struct sockaddr_in serv_addr;
};

#define GATEWAY_ONLINE true
#define GATEWAY_OFFLINE false
#define GATEWAY_PORT_NUMBER 5080
#define SERVER_PORT_NUMBER 8888
#define kMAX_SERVER_IP_LEN 20
#define kMAX_CLIENT_NUMBER 15
#define kCONNECT_REQUEST_INTERVAL 10
#define kGATEWAY_RELEASE 0

void Gateway_Init(int id, char *server_ip);
void Gateway_SendToClient(void *send_buf, size_t buf_size, struct sockaddr_in *client_addr);
void Gateway_SendToServer(void *send_buf, size_t buf_size);
void Gateway_InputId(void);
UInt16 Gateway_GetId(void);
void Gateway_SetStatus(void);
bool Gateway_GetStatus(void);
void Gateway_InitSocket(char *server_ip);
struct socket_info *Gateway_GetSocketInfo(void);
void Gateway_InitConnection(void);
void Gateway_SendHeartbeat(void);
void Gateway_Release(int signo);
void Gateway_Deinit(void);



