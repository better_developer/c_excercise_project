#pragma once
#include <signal.h>
#include <time.h>
#include "type.h"


typedef struct timer {
    timer_t timer_id;
    void *timeout_handler;
    UInt32 seconds;
    struct itimerspec value;
    struct sigevent ent;
}Timer;

void Timer_CreateTimer(Timer *, UInt32, void *);
void Timer_Start(Timer *);
void Timer_DeleteTimer(Timer *);
