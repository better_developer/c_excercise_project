#pragma once
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>

#define kGATEWAY_ID_EXIST 4
#define kFULL_SERVER_LIST 5

void Message_ProcessClientLogin(char *buf, struct sockaddr_in *client_addr);
void Message_ProcessClientLoginResponse(char *buf);
void Message_ProcessGatewayLoginResponse(char *buf);
void Message_ProcessClientReleaseRequest(char *buf);
void Message_ProcessClientHeartbeat(char *buf);
void Message_ProcessClientChat(char *buf);
void Message_ProcessClientChatResponse(char *buf);
