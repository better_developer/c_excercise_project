#pragma once
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <sys/timerfd.h>
#include "type.h"

#define kMAX_CLIENT_NUM 15
#define CLIENT_ONLINE true
#define CLIENT_OFFLINE false
#define CLIENT_EXIST true
#define CLIENT_NOT_EXIST false
#define kTIMEOUT_INTERVAL 10

struct client
{
    UInt16 user_id;
    bool client_status;
    struct sockaddr_in client_addr;
    struct timespec heartbeat_time;
    struct client *next;
};

struct client_info
{
    bool client_status;
    struct sockaddr_in client_addr;
    struct timespec heartbeat_time;
};

void UserList_Init(void);
struct client *UserList_GetHead(void);
bool UserList_Find(UInt16 user_id, struct client_info *info);
void UserList_SetClientStatus(UInt16 user_id);
void UserList_Add(UInt16 user_id, struct sockaddr_in *client_addr);
void UserList_Remove(UInt16 user_id);
void UserList_PrintOnlineUsers(void);
void UserList_CheckHeartbeatTime(void);
void UserList_Deinit(void);
bool UserList_IsFull(void);
bool UserList_ClientIdExist(UInt16 user_id);
void UserList_UpdateHeartbeat(UInt16 user_id);