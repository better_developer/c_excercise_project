from message import *


class Server(object):
    def __init__(self):
        self.server = UDPSocket()


    def start(self, port):
        self.server.start_udp(port)


    def send_response(self, api_id, client_id, status, error_code):
        data_to_send = pack_response(api_id, client_id, status, error_code)
        self.server.send(data_to_send)

    def forward_chat(self, source_client_id, target_client_id, chat_msg_len, chat_msg):
        data_to_send = pack_chat_msg(source_client_id, target_client_id, chat_msg_len, chat_msg)
        self.server.send(data_to_send)

    def receive_heartbeat(self):
        while True:
            data, address = self.server.receive()
            print("Server received a message from ", address)
            api_id = unpack_header(data)
            if api_id == GATEWAY_HEARTBEAT_API_ID:
                client_id = unpack_request(data)
                return api_id, client_id


    def receive_client_connect_request(self):
        while True:
            data, address = self.server.receive()
            print("Server received a message from ", address)
            api_id = unpack_header(data)
            if api_id == CONNECTION_REQUEST_FORWARDING_API_ID:
                client_id = unpack_request(data)
                return api_id, client_id


    def receive_forwarded_connect_request(self):
        while True:
            data, address = self.server.receive()
            print("Server received a message from ", address)
            api_id = unpack_header(data)
            if api_id == CONNECTION_REQUEST_FORWARDING_API_ID:
                client_id, gateway_id = unpack_forwarded_connect_request(data)
                return api_id, client_id, gateway_id


    def receive_gateway_connect_request(self):
        while True:
            data, address = self.server.receive()
            print("Server received a message from ", address)
            api_id = unpack_header(data)
            if api_id == GATEWAY_CONNECTION_REQUEST_API_ID:
                gateway_id = unpack_request(data)
                return api_id, gateway_id


    def receive_client_release_request(self):
        while True:
            data, address = self.server.receive()
            print("Server received a message from ", address)
            api_id = unpack_header(data)
            if api_id == CLIENT_RELEASE_REQUEST_API_ID:
                client_id = unpack_request(data)
                return api_id, client_id


    def receive_gateway_release_request(self):
        while True:
            data, address = self.server.receive()
            print("Server received a message from ", address)
            api_id = unpack_header(data)
            if api_id == GATEWAY_RELEASE_REQUEST_API_ID:
                gateway_id = unpack_request(data)
                return api_id, gateway_id


    def receive_gateway_heartbeat(self):
        while True:
            data, address = self.server.receive()
            print("Server received a message from ", address)
            api_id = unpack_header(data)
            if api_id == GATEWAY_HEARTBEAT_API_ID:
                gateway_id = unpack_request(data)
                return api_id, gateway_id


    def receive_client_release_notice(self):
        while True:
            data, address = self.server.receive()
            print("Server received a message from ", address)
            api_id = unpack_header(data)
            if api_id == CLIENT_RELEASE_NOTICE_FROM_GATEWAY_API_ID:
                client_id = unpack_request(data)
                return api_id, client_id


    def receive_client_chat(self):
        while True:
            data, address = self.server.receive()
            print("Server received a message from ", address)
            api_id = unpack_header(data)
            if api_id == CLIENT_SEND_CHAT_MESSAGE_API_ID:
                return data


    def end(self):
        self.server.close_udp()