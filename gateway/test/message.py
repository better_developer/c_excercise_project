import socket
import struct
from constants import *


class UDPSocket(object):
    def start_udp(self, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((HOST_IP, port))
        print('starting udp on %s' % HOST_IP, port)


    def close_udp(self):
        self.sock.close()
        self.sock = None


    def receive(self):
        data, address = self.sock.recvfrom(BUF_SIZE)
        return data, address


    def send(self, send_data):
        self.sock.sendto(send_data, (GATEWAY_IP, GATEWAY_PORT))


def pack_chat_msg(source_client_id, target_client_id, chat_msg_len, chat_msg):
    data_to_send = struct.pack("!BHHH%ds" % chat_msg_len, CLIENT_SEND_CHAT_MESSAGE_API_ID, source_client_id,
                               target_client_id, chat_msg_len, bytes(chat_msg.encode('utf-8')))
    return data_to_send


def pack_response(api_id, client_id, status, error_code):
    data_to_send = struct.pack("!BHBB", api_id, client_id, status, error_code)
    return data_to_send


def pack_request(api_id, client_id):
    data_to_send = struct.pack("!BH", api_id, client_id)
    return data_to_send


def unpack_header(data):
    api_id = struct.unpack("!B", data[:1])[0]
    return api_id


def unpack_request(data):
    peer_id = struct.unpack("!BH", data[:3])[1]
    return peer_id


def unpack_response(data):
    rec_response = struct.unpack("!BHBB", data[:5])
    client_id = rec_response[1]
    status = rec_response[2]
    error_code = rec_response[3]
    return client_id, status, error_code


def unpack_forwarded_connect_request(data):
    rec_request = struct.unpack("!BHH", data[:5])
    client_id = rec_request[1]
    gateway_id = rec_request[2]
    return client_id, gateway_id


def unpack_chat_msg(data):
    rec_data = struct.unpack("!BHHH", data[:7])
    msg_len = rec_data[3]
    msg = struct.unpack("!%ds" % msg_len, data[7:])
    source_client_id = rec_data[1]
    target_client_id = rec_data[2]
    return source_client_id, target_client_id, msg_len, msg
