import signal
import time
import unittest
import subprocess
from server import *
from client_process import *


def GatewaySetUp(port, ip_address):
    proc = subprocess.Popen(["./../bin/gateway_setup", port, ip_address], shell=False, stdout=subprocess.DEVNULL)
    return proc


def GatewayTearDown(proc):
    proc.send_signal(signal.SIGINT)
    proc.wait()


class GatewayTest(unittest.TestCase):
    def setUp(self):
        self.server = Server()
        self.server.start(8888)
        print('setUp...')


    def tearDown(self):
        self.server.end()
        print('tearDown...')


    def test_gateway_login_successfully(self):
        # Given
        gateway = GatewaySetUp("20", "127.0.0.1")
        expected_gateway_id = 20

        # When
        api_id, gateway_id = self.server.receive_gateway_connect_request()
        self.server.send_response(GATEWAY_CONNECTION_RESPONSE_API_ID, expected_gateway_id, RESPONSE_STATUS_OK, 0)
        GatewayTearDown(gateway)

        # Then
        self.assertEqual(api_id, GATEWAY_CONNECTION_REQUEST_API_ID)
        self.assertEqual(gateway_id, expected_gateway_id)


    def test_gateway_heartbeat(self):
        # Given
        gateway = GatewaySetUp("20", "127.0.0.1")
        receive_heartbeat_times = 0
        expected_time_interval = 20
        expected_gateway_id = 20

        # When
        self.server.receive_gateway_connect_request()
        self.server.send_response(GATEWAY_CONNECTION_RESPONSE_API_ID, expected_gateway_id, RESPONSE_STATUS_OK, 0)

        tick = int(time.time())
        while receive_heartbeat_times < 10:
            self.server.receive_heartbeat()
            receive_heartbeat_times = receive_heartbeat_times + 1
        tock = int(time.time())
        ten_times_heartbeat_interval = tock - tick
        GatewayTearDown(gateway)

        # Then
        self.assertAlmostEqual(ten_times_heartbeat_interval, expected_time_interval, delta=5)


    def test_gateway_setup_with_invalid_id(self):
        # Given
        expected_return_code = INPUT_GATEWAY_ID_INVALID

        # When
        gateway = GatewaySetUp("0", "127.0.0.1")
        return_code = gateway.wait()

        # Then
        self.assertEqual(return_code, expected_return_code)


    def test_gateway_setup_invalid_numbers_of_args(self):
        # Given
        expected_return_code = INPUT_NUMBER_OF_ARGS_INVALID

        # When
        gateway = subprocess.Popen(["./../bin/gateway_setup", "127.0.0.1"], shell=False, stdout=subprocess.DEVNULL)
        return_code = gateway.wait()

        # Then
        self.assertEqual(return_code, expected_return_code)


    def test_gateway_setup_with_invalid_ip_address(self):
        # Given
        expected_return_code = INPUT_SERVER_IP_INVALID

        # When
        gateway = GatewaySetUp("20", "123")
        return_code = gateway.wait()

        # Then
        self.assertEqual(return_code, expected_return_code)


    def test_gateway_login_with_id_exist_err(self):
        # Given
        gateway = GatewaySetUp("20", "127.0.0.1")
        gateway_id = 20
        expected_return_code = GATEWAY_ID_EXIST

        # When
        self.server.receive_gateway_connect_request()
        self.server.send_response(GATEWAY_CONNECTION_RESPONSE_API_ID, gateway_id, RESPONSE_STATUS_ERROR,
                                  CONNECTED_ERR_OF_ID_EXISTED)
        return_code = gateway.wait()

        # Then
        self.assertEqual(return_code, expected_return_code)


    def test_gateway_login_with_full_list(self):
        # Given
        gateway = GatewaySetUp("20", "127.0.0.1")
        gateway_id = 20
        expected_return_code = FULL_SERVER_LIST

        # When
        self.server.receive_gateway_connect_request()
        self.server.send_response(GATEWAY_CONNECTION_RESPONSE_API_ID, gateway_id, RESPONSE_STATUS_ERROR,
                                  CONNECTED_ERR_OF_LIST_FULL)
        return_code = gateway.wait()

        # Then
        self.assertEqual(return_code, expected_return_code)


    def test_gateway_receive_invalid_api(self):
        # Given
        gateway_id = 20
        gateway = GatewaySetUp("20", "127.0.0.1")
        self.server.receive_gateway_connect_request()
        self.server.send_response(GATEWAY_CONNECTION_RESPONSE_API_ID, gateway_id, RESPONSE_STATUS_OK, 0)

        # When
        self.server.send_response(INVALID_API, gateway_id, RESPONSE_STATUS_OK, 0)
        time.sleep(1)
        GatewayTearDown(gateway)

        # Then


    def test_gateway_release(self):
        # Given
        gateway = GatewaySetUp("20", "127.0.0.1")
        client = Client()
        client.start(7000)
        client_id = 22001
        expected_return_code = GATEWAY_RELEASE
        expected_gateway_id = 20

        # When
        self.server.receive_gateway_connect_request()
        self.server.send_response(GATEWAY_CONNECTION_RESPONSE_API_ID, expected_gateway_id, RESPONSE_STATUS_OK, 0)

        client.send_request(CLIENT_CONNECTION_REQUEST_API_ID, client_id)
        self.server.receive_client_connect_request()
        self.server.send_response(CLIENT_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_OK, 0)
        client.receive_client_connect_response()

        time.sleep(5)
        gateway.send_signal(signal.SIGINT)
        server_recv_api_id, gateway_id = self.server.receive_gateway_release_request()
        client_recv_api_id = client.receive_gateway_release_notice()
        return_code = gateway.wait()
        client.end()

        # Then
        self.assertEqual(return_code, expected_return_code)
        self.assertEqual(server_recv_api_id, GATEWAY_RELEASE_REQUEST_API_ID)
        self.assertEqual(gateway_id, expected_gateway_id)
        self.assertEqual(client_recv_api_id, GATEWAY_RELEASE_NOTICE_API_ID)


if __name__ == '__main__':
    unittest.main(verbosity=2)
