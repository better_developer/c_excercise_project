from message import *
from constants import *


class Client(object):
    def __init__(self):
        self.client = UDPSocket()
        self.client_id = 0


    def start(self, port):
        self.client.start_udp(port)


    def send_request(self, api_id, client_id):
        data_to_send = pack_request(api_id, client_id)
        self.client.send(data_to_send)


    def send_heartbeat(self, client_id):
        data_to_send = pack_request(CLIENT_HEARTBEAT_API_ID, client_id)
        self.client.send(data_to_send)


    def chat(self, source_client_id, target_client_id, chat_msg_len, chat_msg):
        data_to_send = pack_chat_msg(source_client_id, target_client_id, chat_msg_len, chat_msg)
        self.client.send(data_to_send)


    def receive_client_connect_response(self):
        while True:
            data, address = self.client.receive()
            print("Client received a message from ", address)
            api_id = unpack_header(data)
            if api_id == CLIENT_CONNECTION_RESPONSE_API_ID:
                client_id, status, error_code = unpack_response(data)
                if status == RESPONSE_STATUS_OK:
                    self.client_id = client_id
                return api_id, client_id, status, error_code


    def receive_gateway_release_notice(self):
        while True:
            data, address = self.client.receive()
            print("Client received a message from ", address)
            api_id = unpack_header(data)
            if api_id == GATEWAY_RELEASE_NOTICE_API_ID:
                return api_id


    def receive_chat(self):
        while True:
            data, address = self.client.receive()
            print("Client received a message from ", address)
            api_id = unpack_header(data)
            if api_id == CLIENT_SEND_CHAT_MESSAGE_API_ID:
                return data


    def receive_chat_response(self):
        while True:
            data, address = self.client.receive()
            print("Client received a message from ", address)
            api_id = unpack_header(data)
            if api_id == CHAT_MESSAGE_RESPONSE_API_ID:
                client_id, status, error_code = unpack_response(data)
                return api_id, client_id, status, error_code


    def end(self):
        self.client.close_udp()
