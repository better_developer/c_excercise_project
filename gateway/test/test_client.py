import time
import unittest
import subprocess
from server import *
from client_process import *
from constants import *
import signal

server = Server()
Clients = []
for j in range(MAX_CLIENTS_NUMBER):
    Clients.append(Client())
gateway_setup = subprocess.Popen(["./../bin/gateway_setup", "20", "127.0.0.1"], shell=False, stdout=subprocess.DEVNULL)


def gatewayLogin():
    server.receive_gateway_connect_request()
    server.send_response(GATEWAY_CONNECTION_RESPONSE_API_ID, 20, RESPONSE_STATUS_OK, 0)


def clientLogin(index, client_id):
    Clients[index].send_request(CLIENT_CONNECTION_REQUEST_API_ID, client_id)
    server.receive_client_connect_request()
    server.send_response(CLIENT_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_OK, 0)
    Clients[index].receive_client_connect_response()


def clientGroupStart(num):
    for i in range(num):
        Clients[i].start(7000 + i)


def clientGroupEnd(num):
    for i in range(num):
        Clients[i].send_request(CLIENT_RELEASE_REQUEST_API_ID, Clients[i].client_id)
        Clients[i].end()


def setUpModule():
    server.start(8888)
    gatewayLogin()
    print('setUp module...')


def tearDownModule():
    print('tearDown module...')
    server.end()
    gateway_setup.send_signal(signal.SIGINT)
    gateway_setup.wait()


class ClientTest(unittest.TestCase):
    def setUp(self):
        print('setUp...')


    def tearDown(self):
        print('tearDown...')


    def test_client_login_successfully(self):
        # Given
        expected_client_s_id = 22001
        expected_client_t_id = 22002
        clientGroupStart(2)

        # When
        Clients[0].send_request(CLIENT_CONNECTION_REQUEST_API_ID, expected_client_s_id)
        Clients[1].send_request(CLIENT_CONNECTION_REQUEST_API_ID, expected_client_t_id)
        rev_api_id, rev_client_id = server.receive_client_connect_request()
        server.send_response(CLIENT_CONNECTION_RESPONSE_API_ID, expected_client_s_id, RESPONSE_STATUS_OK, 0)
        resp_api_id, resp_client_id, resp_status, resp_error_code = Clients[0].receive_client_connect_response()
        clientGroupEnd(2)

        # Then
        self.assertEqual(resp_api_id, CLIENT_CONNECTION_RESPONSE_API_ID)
        self.assertEqual(resp_client_id, expected_client_s_id)
        self.assertEqual(resp_status, RESPONSE_STATUS_OK)
        self.assertEqual(resp_error_code, 0)
        self.assertEqual(rev_api_id, CONNECTION_REQUEST_FORWARDING_API_ID)
        self.assertEqual(rev_client_id, expected_client_s_id)


    def test_client_login_with_id_exist_in_gateway_err(self):
        # Given
        expected_client_id = 22001
        clientGroupStart(2)

        # When
        Clients[0].send_request(CLIENT_CONNECTION_REQUEST_API_ID, expected_client_id)
        server.send_response(CLIENT_CONNECTION_RESPONSE_API_ID, expected_client_id, RESPONSE_STATUS_OK, 0)
        Clients[1].send_request(CLIENT_CONNECTION_REQUEST_API_ID, expected_client_id)
        resp_api_id, resp_client_id, resp_status, resp_error_code = Clients[1].receive_client_connect_response()
        clientGroupEnd(2)

        # Then
        self.assertEqual(resp_api_id, CLIENT_CONNECTION_RESPONSE_API_ID)
        self.assertEqual(resp_client_id, expected_client_id)
        self.assertEqual(resp_status, RESPONSE_STATUS_ERROR)
        self.assertEqual(resp_error_code, CONNECTED_ERR_OF_ID_EXISTED)


    def test_client_login_with_id_exist_in_server_err(self):
        expected_client_id = 22001
        clientGroupStart(1)

        # When
        Clients[0].send_request(CLIENT_CONNECTION_REQUEST_API_ID, expected_client_id)
        server.send_response(CLIENT_CONNECTION_RESPONSE_API_ID, expected_client_id, RESPONSE_STATUS_ERROR,
                             CONNECTED_ERR_OF_ID_EXISTED)
        resp_api_id, resp_client_id, resp_status, resp_error_code = Clients[0].receive_client_connect_response()
        clientGroupEnd(1)

        # Then
        self.assertEqual(resp_api_id, CLIENT_CONNECTION_RESPONSE_API_ID)
        self.assertEqual(resp_client_id, expected_client_id)
        self.assertEqual(resp_status, RESPONSE_STATUS_ERROR)
        self.assertEqual(resp_error_code, CONNECTED_ERR_OF_ID_EXISTED)


    def test_client_login_with_full_list_in_gateway_err(self):
        # Given
        expected_client_id = 22016
        clientGroupStart(MAX_CLIENTS_NUMBER)

        # When
        for i in range(MAX_CLIENTS_NUMBER - 1):
            clientLogin(i, 22001 + i)
        Clients[MAX_CLIENTS_NUMBER - 1].send_request(CLIENT_CONNECTION_REQUEST_API_ID, expected_client_id)
        resp_api_id, resp_client_id, resp_status, resp_error_code = \
            Clients[MAX_CLIENTS_NUMBER - 1].receive_client_connect_response()
        clientGroupEnd(MAX_CLIENTS_NUMBER)

        # Then
        self.assertEqual(resp_api_id, CLIENT_CONNECTION_RESPONSE_API_ID)
        self.assertEqual(resp_client_id, expected_client_id)
        self.assertEqual(resp_status, RESPONSE_STATUS_ERROR)
        self.assertEqual(resp_error_code, CONNECTED_ERR_OF_LIST_FULL)


    def test_client_login_with_full_list_in_server_err(self):
        # Given
        expected_client_id = 22001
        clientGroupStart(1)

        # When
        Clients[0].send_request(CLIENT_CONNECTION_REQUEST_API_ID, expected_client_id)
        server.send_response(CLIENT_CONNECTION_RESPONSE_API_ID, expected_client_id, RESPONSE_STATUS_ERROR,
                             CONNECTED_ERR_OF_LIST_FULL)
        resp_api_id, resp_client_id, resp_status, resp_error_code = Clients[0].receive_client_connect_response()
        clientGroupEnd(1)

        # Then
        self.assertEqual(resp_api_id, CLIENT_CONNECTION_RESPONSE_API_ID)
        self.assertEqual(resp_client_id, expected_client_id)
        self.assertEqual(resp_status, RESPONSE_STATUS_ERROR)
        self.assertEqual(resp_error_code, CONNECTED_ERR_OF_LIST_FULL)


    def test_client_send_more_than_one_connect_request(self):
        # Given
        expected_client_id = 22001
        request_times = 5
        clientGroupStart(1)

        # When
        for i in range(request_times):
            Clients[0].send_request(CLIENT_CONNECTION_REQUEST_API_ID, expected_client_id)
            server.receive_client_connect_request()
        server.send_response(CLIENT_CONNECTION_RESPONSE_API_ID, expected_client_id, RESPONSE_STATUS_OK, 0)
        resp_api_id, resp_client_id, resp_status, resp_error_code = Clients[0].receive_client_connect_response()
        clientGroupEnd(1)

        # Then
        self.assertEqual(resp_api_id, CLIENT_CONNECTION_RESPONSE_API_ID)
        self.assertEqual(resp_client_id, expected_client_id)
        self.assertEqual(resp_status, RESPONSE_STATUS_OK)
        self.assertEqual(resp_error_code, 0)


    def test_client_chat_under_gateway_successfully(self):
        # Given
        expected_source_client_id = 22001
        expected_target_client_id = 22002
        expected_chat_msg = "Hello"
        expected_data = pack_chat_msg(expected_source_client_id, expected_target_client_id, len(expected_chat_msg),
                                      expected_chat_msg)
        clientGroupStart(2)
        for i in range(2):
            clientLogin(i, 22001 + i)

        # When
        Clients[0].chat(expected_source_client_id, expected_target_client_id, len(expected_chat_msg), expected_chat_msg)
        data = Clients[1].receive_chat()
        clientGroupEnd(2)

        # Then
        self.assertEqual(data, expected_data)


    def test_client_chat_from_gateway_to_server_successfully(self):
        # Given
        expected_source_client_id = 22001
        expected_target_client_id = 22002
        expected_chat_msg = "Hello"
        clientGroupStart(1)
        expected_data = pack_chat_msg(expected_source_client_id, expected_target_client_id, len(expected_chat_msg),
                                      expected_chat_msg)
        clientLogin(0, expected_source_client_id)

        # When
        Clients[0].chat(expected_source_client_id, expected_target_client_id, len(expected_chat_msg), expected_chat_msg)
        data = server.receive_client_chat()
        server.send_response(CHAT_MESSAGE_RESPONSE_API_ID, expected_source_client_id, RESPONSE_STATUS_OK, 0)
        api_id, client_id, status, error_code = Clients[0].receive_chat_response()
        clientGroupEnd(1)

        # Then
        self.assertEqual(data, expected_data)
        self.assertEqual(api_id, CHAT_MESSAGE_RESPONSE_API_ID)
        self.assertEqual(client_id, expected_source_client_id)
        self.assertEqual(status, RESPONSE_STATUS_OK)
        self.assertEqual(error_code, 0)


    def test_client_chat_from_server_to_gateway_successfully(self):
        # Given
        expected_source_client_id = 22001
        expected_target_client_id = 22002
        expected_chat_msg = "Hello"
        clientGroupStart(1)
        expected_data = pack_chat_msg(expected_source_client_id, expected_target_client_id, len(expected_chat_msg),
                                      expected_chat_msg)
        clientLogin(0, expected_target_client_id)

        # When
        server.forward_chat(expected_source_client_id, expected_target_client_id, len(expected_chat_msg),
                            expected_chat_msg)
        data = Clients[0].receive_chat()
        clientGroupEnd(1)

        # Then
        self.assertEqual(data, expected_data)


    def test_client_chat_unsuccessfully(self):
        # Given
        expected_source_client_id = 22001
        expected_target_client_id = 22002
        expected_chat_msg = "Hello"
        clientGroupStart(1)
        expected_data = pack_chat_msg(expected_source_client_id, expected_target_client_id, len(expected_chat_msg),
                                      expected_chat_msg)
        clientLogin(0, expected_source_client_id)

        # When
        Clients[0].chat(expected_source_client_id, expected_target_client_id, len(expected_chat_msg), expected_chat_msg)
        data = server.receive_client_chat()
        server.send_response(CHAT_MESSAGE_RESPONSE_API_ID, expected_source_client_id, RESPONSE_STATUS_ERROR,
                             TARGET_CLIENT_OFFLINE_ERR)
        api_id, client_id, status, error_code = Clients[0].receive_chat_response()
        clientGroupEnd(1)

        # Then
        self.assertEqual(data, expected_data)
        self.assertEqual(api_id, CHAT_MESSAGE_RESPONSE_API_ID)
        self.assertEqual(client_id, expected_source_client_id)
        self.assertEqual(status, RESPONSE_STATUS_ERROR)
        self.assertEqual(error_code, TARGET_CLIENT_OFFLINE_ERR)


    def test_client_release(self):
        # Given
        expected_client_id = 22001
        clientGroupStart(1)
        clientLogin(0, expected_client_id)

        # When
        Clients[0].send_request(CLIENT_RELEASE_REQUEST_API_ID, Clients[0].client_id)
        api_id, client_id = server.receive_client_release_request()
        Clients[0].end()

        # Then
        self.assertEqual(api_id, CLIENT_RELEASE_REQUEST_API_ID)
        self.assertEqual(client_id, expected_client_id)


    def test_client_without_heartbeat(self):
        # Given
        expected_client_id = 22001
        expected_heartbeat_timeout_interval = 10
        clientGroupStart(1)
        clientLogin(0, expected_client_id)

        # When
        tick = int(time.time())
        api_id, client_id = server.receive_client_release_notice()
        tock = int(time.time())
        heartbeat_timeout_interval = tock - tick
        clientGroupEnd(1)

        # Then
        self.assertEqual(api_id, CLIENT_RELEASE_NOTICE_FROM_GATEWAY_API_ID)
        self.assertEqual(client_id, expected_client_id)
        self.assertAlmostEqual(heartbeat_timeout_interval, expected_heartbeat_timeout_interval, delta=4)


    def test_client_heartbeat(self):
        # Given
        expected_target_client_id = 22001
        expected_source_client_id = 22002
        expected_chat_msg = "Hello"
        send_heartbeat_times = 0
        expected_data = pack_chat_msg(expected_source_client_id, expected_target_client_id, len(expected_chat_msg),
                                      expected_chat_msg)
        clientGroupStart(2)
        clientLogin(0, expected_target_client_id)
        clientLogin(1, expected_source_client_id)

        # When
        while send_heartbeat_times < 10:
            Clients[0].send_heartbeat(expected_target_client_id)
            Clients[1].send_heartbeat(expected_source_client_id)
            time.sleep(2)
            send_heartbeat_times = send_heartbeat_times + 1

        Clients[1].chat(expected_source_client_id, expected_target_client_id, len(expected_chat_msg), expected_chat_msg)
        data = Clients[0].receive_chat()
        clientGroupEnd(2)

        # Then
        self.assertEqual(data, expected_data)


if __name__ == '__main__':
    unittest.main(verbosity=2)
