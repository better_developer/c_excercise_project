#pragma once
#include "type.h"

struct common_message_header {
    UInt8 api_id;
};

struct common_info_body {
    UInt16 client_id_or_gateway_id;
};

struct common_info {
    struct common_message_header header;
    struct common_info_body body;
} __attribute__((packed));

struct client_connect_request_forwarding_body {
    UInt16 client_id;
    UInt16 gateway_id;
};

struct client_connect_requset_forwarding {
    struct common_message_header header;
    struct client_connect_request_forwarding_body body;
} __attribute__((packed));

struct common_response_body {
    UInt16 client_id_or_gateway_id;
    UInt8 status;
    UInt8 error_code;
};

struct common_reponse {
    struct common_message_header header;
    struct common_response_body body;
} __attribute__((packed));

struct chat_message_from_client_body {
    UInt16 source_client_id;
    UInt16 destination_client_id;
    UInt16 chat_msg_len;
    char chat_msg[0];
};

struct chat_message_from_client {
    struct common_message_header header;
    struct chat_message_from_client_body body;
} __attribute__((packed));

struct client_release_notice_from_gateway_body {
    UInt16 client_id;
};

struct client_release_notice_from_gateway {
    struct common_message_header header;
    struct client_release_notice_from_gateway_body body;
} __attribute__((packed));
