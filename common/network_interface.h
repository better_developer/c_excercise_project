#pragma once

#include <arpa/inet.h>

#define IPADDR_MAX_SIZE 20

typedef struct network_interface NetworkInterface;

typedef void (*NetworkInterface_Connect_Ptr) (NetworkInterface * const, const char * const, const unsigned short int);
typedef int (*NetworkInterface_Send_Ptr) (NetworkInterface * const, void * const, const size_t);
typedef int (*NetworkInterface_Bind_Ptr) (NetworkInterface * const, const char * const, unsigned short int);
typedef int (*NetworkInterface_Receive_Ptr) (NetworkInterface * const, const size_t, void *, struct sockaddr_in *);


struct network_interface{
    int sockfd;
    char connect_remote_ip_addr[IPADDR_MAX_SIZE];
    unsigned short connect_remote_port;

    NetworkInterface_Connect_Ptr NetworkInterface_Connect;
    NetworkInterface_Send_Ptr NetworkInterface_Send;
    NetworkInterface_Bind_Ptr NetworkInterface_Bind;
    NetworkInterface_Receive_Ptr NetworkInterface_Receive;
};

NetworkInterface *NetworkInterface_CreateSocket(void);
void NetworkInterface_DeleteSocket(NetworkInterface *);
