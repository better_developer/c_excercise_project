#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "network_interface.h"

static void SocketConnect(NetworkInterface * const self, const char * const ip, const unsigned short int port)
{
    memcpy(self->connect_remote_ip_addr, ip, strlen(ip)+1);
    self->connect_remote_port = port;
}


static int SocketSend(NetworkInterface * const self, void * const sendbuf, const size_t buf_size)
{
    int ret = 0;
    struct sockaddr_in remote_addr = {0};

    remote_addr.sin_family = AF_INET;
    remote_addr.sin_addr.s_addr = inet_addr(self->connect_remote_ip_addr);
    remote_addr.sin_port = htons(self->connect_remote_port);
    ret = sendto(self->sockfd, sendbuf, buf_size, 0, (struct sockaddr *) &remote_addr, sizeof(remote_addr));

    return ret;
}


static int SocketBind(NetworkInterface * const self, const char * const ip, unsigned short int port)
{
    struct sockaddr_in local_addr = {0};

    local_addr.sin_family = AF_INET;
    local_addr.sin_addr.s_addr = inet_addr(ip);
    local_addr.sin_port = htons(port);

    return bind(self->sockfd, (struct sockaddr *) &local_addr, sizeof(local_addr));
}


static int SocketReceive(NetworkInterface * const self, const size_t buf_size, void *recvbuf, struct sockaddr_in *recv_addr)
{
    int ret = 0;
    socklen_t addrlen = sizeof(recv_addr);

    ret = recvfrom(self->sockfd, recvbuf, buf_size, 0, (struct sockaddr *) recv_addr, &addrlen);

    return ret;
}


NetworkInterface *NetworkInterface_CreateSocket(void)
{
    NetworkInterface *NetworkInterface_socket = malloc(sizeof(NetworkInterface));

    if (NetworkInterface_socket == NULL) {
        return NULL;
    }

    NetworkInterface_socket->NetworkInterface_Connect = SocketConnect;
    NetworkInterface_socket->NetworkInterface_Send = SocketSend;
    NetworkInterface_socket->NetworkInterface_Bind = SocketBind;
    NetworkInterface_socket->NetworkInterface_Receive= SocketReceive;

    NetworkInterface_socket->sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    return NetworkInterface_socket;
}


void NetworkInterface_DeleteSocket(NetworkInterface *self)
{
    close(self->sockfd);
    free(self);
}
