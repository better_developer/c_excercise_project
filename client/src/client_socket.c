#include <assert.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include "network_interface.h"
#include "client_constants.h"
#include "client_socket.h"

static NetworkInterface *client_socket = NULL;


static int isValidIp(const char *ip_str)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ip_str, &(sa.sin_addr));

    return result;
}


void ClientSocket_Init(char *client_ip)
{
    char remote_ip[IPADDR_MAX_SIZE] = {0};

    assert(client_ip);

    client_socket = NetworkInterface_CreateSocket();
    ClientSocket_Bind(client_ip, 0);

    printf("Input remote ip address: ");
    scanf("%s", remote_ip);

    ClientSocket_Connect(remote_ip, kREMOTE_PORT);
}


void ClientSocket_Connect(const char * const ip, const UInt16 port)
{
    if (isValidIp(ip) == kCLIENT_SOCKET_INVALID_IP) {
        printf("Connect ip is invalid!\n");
        exit(2);
    }

    client_socket->NetworkInterface_Connect(client_socket, ip, port);

    printf("Connect %s:%hu successful!\n", ip, port);
}


void ClientSocket_Bind(const char * const ip, UInt16 port)
{
    int ret = client_socket->NetworkInterface_Bind(client_socket, ip, port);

    if (ret == kCLIENT_SOCKET_BIND_FAILURE) {
        printf("Bind failure!\n");
        exit(2);
    }

    printf("Bind %s:%hu successful!\n", ip, port);
}


int ClientSocket_Send(void * const sendbuf, const size_t buf_size)
{
    int ret = client_socket->NetworkInterface_Send(client_socket, sendbuf, buf_size);

    return ret;
}


int ClientSocket_Receive(const size_t buf_size, void *recv_buf, struct sockaddr_in *recv_addr)
{
    int ret = client_socket->NetworkInterface_Receive(client_socket, buf_size, recv_buf, recv_addr);

    return ret;
}


void ClientSocket_Deinit(void)
{
    NetworkInterface_DeleteSocket(client_socket);
}
