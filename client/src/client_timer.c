#include "client_constants.h"
#include "message.h"
#include "timer.h"
#include "client_timer.h"

static Timer connection_timer;
static Timer heartbeat_timer;


void ClientTimer_Init(void)
{
    Timer_CreateTimer(&connection_timer, CONNECTION_DEFAULT_TIME, Message_SendConnectRequset);
    Timer_CreateTimer(&heartbeat_timer, HEARTBEAT_DEFAULT_TIME, Message_SendHeartbeat);
}


void ClientTimer_Deinit(void)
{
    ClientTimer_StopConnectionTimer();
    ClientTimer_StopHeartbeatTimer();
}


void ClientTimer_StartConnectionTimer(void)
{
    Timer_Start(&connection_timer);
}


void ClientTimer_StartHeartbeatTimer(void)
{
    Timer_Start(&heartbeat_timer);
}


void ClientTimer_StopConnectionTimer(void)
{
    Timer_DeleteTimer(&connection_timer);
}


void ClientTimer_StopHeartbeatTimer(void)
{
    Timer_DeleteTimer(&heartbeat_timer);
}
