#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "response_status.h"
#include "api.h"
#include "client_constants.h"
#include "message_format.h"
#include "message.h"
#include "client_socket.h"
#include "client_timer.h"
#include "client.h"
#include "listener.h"


static void receiveConnectionResponseProcess(const void * const recvbuf)
{
    struct common_reponse client_connect_response_data;

    Message_UnpackClientConnectResponseData(recvbuf, &client_connect_response_data);

    if (client_connect_response_data.body.status == kRESPONSE_STATUS_ERROR) {
        printf(kCONNECTION_FAILURE_INFO);
        Client_Deinit();
    }

    printf(kCONNECTION_SUCCESSFUL_INFO);
    ClientTimer_StopConnectionTimer();
    ClientTimer_StartHeartbeatTimer();
    Client_SetConnectionStatus(kCLIENT_CONNECTED_STATUS);
}


static void receiveChatMessageProcess(const void * const recvbuf)
{
    struct chat_message_from_client *data = (struct chat_message_from_client *) recvbuf;

    printf("\r>> %d say: %s\n", ntohs(data->body.source_client_id), data->body.chat_msg);
    printf("<<");
    fflush(stdout);
}


static void receiveChatMessageResponseProcess(const void * const recvbuf)
{
    struct common_reponse client_chat_message_response_data;

    Message_UnpackClientChatMessageResponseData(recvbuf, &client_chat_message_response_data);

    if (client_chat_message_response_data.body.status == kRESPONSE_STATUS_ERROR) {
        printf(kSEND_CHAT_MSG_FAILURE_INFO);
    }
}


void *Listener_Receive(void *data)
{
    while (1) {
        char recvbuf[kRECEIVE_BUFFER_SIZE] = {0};
        struct sockaddr_in recv_addr;
        struct common_message_header *recv_header = (struct common_message_header *)recvbuf;

        ClientSocket_Receive(kRECEIVE_BUFFER_SIZE, recvbuf, &recv_addr);

        switch (recv_header->api_id)
        {
        case CLIENT_CONNECTION_RESPONSE_API_ID: {
                receiveConnectionResponseProcess(recvbuf);
                break;
            }

        case CLIENT_SEND_CHAT_MESSAGE_API_ID: {
                receiveChatMessageProcess(recvbuf);
                break;
            }

        case CHAT_MESSAGE_RESPONSE_API_ID: {
                receiveChatMessageResponseProcess(recvbuf);
                break;
            }

        case GATEWAY_RELEASE_NOTICE_API_ID: {
                printf(kRECEIVE_GATEWAY_RELEASE_NOTIFICATION_INFO);
                Client_Deinit();
            }

        default:
            printf(kRECEIVE_INVALID_API_ID);
            break;
        }
    }
}
