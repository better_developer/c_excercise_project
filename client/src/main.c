#include <stdio.h>
#include <stdlib.h>
#include "client.h"


int main(int argc, char *argv[])
{
    char *client_ip;

    if (argc > 2) {
        printf("Illegal input: Enter more than one.\n");
        exit(2);
    } else if (argc == 2) {
        client_ip = argv[1];
    } else {
        client_ip = "0.0.0.0";
    }

    Client_Init(client_ip);
    Client_StartConnection();
    Client_StartConversation();

    return 0;
}
