#include <string.h>
#include <stdlib.h>
#include "response_status.h"
#include "client_socket.h"
#include "client.h"
#include "api.h"
#include "message.h"


static void packConnectRequestData(struct common_info *connect_request_data, unsigned short client_id)
{
    connect_request_data->header.api_id = CLIENT_CONNECTION_REQUEST_API_ID;
    connect_request_data->body.client_id_or_gateway_id = htons(client_id);
}


static void packReleaseRequestData(struct common_info *release_request_data, unsigned short client_id)
{
    release_request_data->header.api_id = CLIENT_RELEASE_REQUEST_API_ID;
    release_request_data->body.client_id_or_gateway_id = htons(client_id);
}


static void packHeartbeatData(struct common_info *heartbeat_data, unsigned short client_id)
{
    heartbeat_data->header.api_id = CLIENT_HEARTBEAT_API_ID;
    heartbeat_data->body.client_id_or_gateway_id = htons(client_id);
}


static struct chat_message_from_client *packChatMessageData(unsigned short source_client_id, unsigned short dest_client_id, const void *chat_msg)
{
    struct chat_message_from_client *chat_message_data = malloc(sizeof(struct chat_message_from_client) + strlen(chat_msg) + 1);

    chat_message_data->header.api_id = CLIENT_SEND_CHAT_MESSAGE_API_ID;
    chat_message_data->body.source_client_id =htons(source_client_id);
    chat_message_data->body.destination_client_id = htons(dest_client_id);
    memcpy(chat_message_data->body.chat_msg, chat_msg, strlen(chat_msg) + 1);
    chat_message_data->body.chat_msg_len = htons(strlen(chat_msg) + 1);

    return chat_message_data;
}


void Message_UnpackClientConnectResponseData(const void *const recvbuf, struct common_reponse *data)
{
    memcpy(data, recvbuf, sizeof(struct common_reponse));
}


void Message_UnpackClientChatMessageResponseData(const void *const recvbuf, struct common_reponse *data)
{
    memcpy(data, recvbuf, sizeof(struct common_reponse));
}


void Message_SendConnectRequset(void)
{
    struct common_info connect_request_data;
    unsigned short client_id = Client_GetId();

    packConnectRequestData(&connect_request_data, client_id);
    ClientSocket_Send(&connect_request_data, sizeof(connect_request_data));
}


void Message_SendReleaseRequset(void)
{
    struct common_info release_request_data;
    unsigned short client_id = Client_GetId();

    packReleaseRequestData(&release_request_data, client_id);
    ClientSocket_Send(&release_request_data, sizeof(release_request_data));
}


void Message_SendHeartbeat(void)
{
    struct common_info heartbeat_data;
    unsigned short client_id = Client_GetId();

    packHeartbeatData(&heartbeat_data, client_id);
    ClientSocket_Send(&heartbeat_data, sizeof(heartbeat_data));
}


void Message_SendChatMessage(unsigned short dest_client_id, const void *chat_msg)
{
    unsigned short client_id = Client_GetId();
    struct chat_message_from_client *chat_message_data = packChatMessageData(client_id, dest_client_id, chat_msg);

    ClientSocket_Send(chat_message_data, sizeof(struct chat_message_from_client) + strlen(chat_msg) + 1);
    free(chat_message_data);
}
