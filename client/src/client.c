#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include "client_constants.h"
#include "message.h"
#include "client_socket.h"
#include "client_timer.h"
#include "client.h"
#include "listener.h"

static UInt16 client_id;
static UInt16 connection_status;
static pthread_t thread_receive_pid;

static void inputClientId(void)
{
    printf("Please input your Client Id: ");
    scanf("%hd", &client_id);
    printf("client_id = %d\n", client_id);
}


static void quitClient(void)
{
    Message_SendReleaseRequset();
    ClientSocket_Deinit();
    ClientTimer_Deinit();
    printf("exit!\n");

    pthread_cancel(thread_receive_pid);
    pthread_join(thread_receive_pid, NULL);

    exit(1);
}


static void handleSigInt(int sig)
{
    quitClient();
}


void Client_StartConnection(void)
{
    inputClientId();
    Message_SendConnectRequset();
    ClientTimer_StartConnectionTimer();
}


void Client_Init(char *client_ip)
{


    connection_status = kCLIENT_DISCONNECTED_STATUS;
    ClientSocket_Init(client_ip);
    ClientTimer_Init();
    signal(SIGINT, handleSigInt);

    pthread_create(&thread_receive_pid, NULL, Listener_Receive, NULL);
}


void Client_Deinit(void)
{
    ClientSocket_Deinit();
    ClientTimer_Deinit();
    printf("exit!\n");

    pthread_join(thread_receive_pid, NULL);

    exit(1);
}


UInt16 Client_GetId(void)
{
    return client_id;
}


void Client_SetConnectionStatus(UInt16 status)
{
    connection_status = status;
}


static void chooseClientToChat(void)
{
    SInt8 send_buf[kCHAT_MSG_SIZE] = {0};
    UInt16 dest_client_id;

    printf("Enter client id to chat or input 0 to quit:\n<<");
    scanf("%hu", &dest_client_id);
    getchar();

    if (dest_client_id == 0) {
        quitClient();
    }

    printf("Please enter what you want to send:\n<<");
    fgets(send_buf, kCHAT_MSG_SIZE, stdin);

    Message_SendChatMessage(dest_client_id, send_buf);
}


void Client_StartConversation(void)
{
    while (1) {
        sleep(1);
        if (connection_status == kCLIENT_CONNECTED_STATUS) {
            chooseClientToChat();
        }
    }
}
