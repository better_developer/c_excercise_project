import unittest
import signal

from message_service import *
from constanst import *
from test_service import *


class ClientTest(unittest.TestCase):
    def __init__(self, method_name):
        unittest.TestCase.__init__(self, method_name)
        server_socket


    def setUp(self):
        server_socket.start_udp(SERVER_IP, SERVER_PORT)


    def tearDown(self):
        server_socket.close_udp()


    def test_recv_correct_connect_request(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_CORRECT_CONNECTION_INPUT)

        data, addr = server_socket.receive()

        api_id = unpack_common_message_header(data)
        recv_client_id= connection_process(data, addr)

        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(api_id, CLIENT_CONNECTION_REQUEST_API_ID)
        self.assertEqual(return_code, TEST_EXPECTED_RETURN_CODE)


    def test_recv_unexpected_connect_request(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_UNEXPECTED_CONNECTION_INPUT)

        data, addr = server_socket.receive()

        api_id = unpack_common_message_header(data)
        recv_client_id= connection_process(data, addr)

        return_code = stop_cient(child)

        # Then
        self.assertNotEqual(recv_client_id, expected_client_id)
        self.assertEqual(api_id, CLIENT_CONNECTION_REQUEST_API_ID)
        self.assertEqual(return_code, TEST_EXPECTED_RETURN_CODE)


    def test_connet_invalid_remote_ip(self):
        # Given
        expected_return_code = 2

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_CONNECT_INVALID_REMOTE_IP_INPUT)

        return_code = stop_cient(child)

        #Then
        self.assertEqual(return_code, expected_return_code)


    def test_too_more_argv(self):
        # Given
        expected_return_code = 2

        # When
        child = start_client(TEST_INVALID_STARTUP_CONMAND, '')

        return_code = stop_cient(child)

        # Then
        self.assertEqual(return_code, expected_return_code)


    def test_input_valid_all_ipv4_addr(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID

        # When
        child = start_client(TEST_VALID_ALL_IPV4_ADDR_STARTUP_CONMAND, TEST_CORRECT_CONNECTION_INPUT)

        data, addr = server_socket.receive()

        api_id = unpack_common_message_header(data)
        recv_client_id = connection_process(data, addr)
        ip_addr = addr[0]
        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(api_id, CLIENT_CONNECTION_REQUEST_API_ID)
        self.assertEqual(return_code, TEST_EXPECTED_RETURN_CODE)
        self.assertEqual(ip_addr, TEST_IP_ADDR)


    def test_input_valid_client_local_ip(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID

        # When
        child = start_client(TEST_VALID_CLIENT_LOCAL_IP_STARTUP_CONMAND, TEST_CORRECT_CONNECTION_INPUT)

        data, addr = server_socket.receive()

        api_id = unpack_common_message_header(data)
        recv_client_id = connection_process(data, addr)
        ip_addr = addr[0]
        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(api_id, CLIENT_CONNECTION_REQUEST_API_ID)
        self.assertEqual(return_code, TEST_EXPECTED_RETURN_CODE)
        self.assertEqual(ip_addr, TEST_IP_ADDR)


    def test_input_invalid_client_ip(self):
        # Given
        expected_return_code = 2
        # When
        child = start_client(TEST_INVALID_CLIENT_IP_STARTUP_CONMAND, '')

        return_code = stop_cient(child)

        # Then
        self.assertEqual(return_code, expected_return_code)


    def test_recv_sigint(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID
        expected_return_code = 1

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_RECV_SIGINT_INPUT)

        data, addr = server_socket.receive()

        api_id = unpack_common_message_header(data)
        recv_client_id = connection_process(data, addr)

        child.send_signal(signal.SIGINT)
        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(api_id, CLIENT_CONNECTION_REQUEST_API_ID)
        self.assertEqual(return_code, expected_return_code)


    def test_recv_client_heartbeat(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID
        expected_return_code = 1

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_RECV_CLIENT_HEARTBEAT_INPUT)

        data, addr = server_socket.receive()
        recv_client_id = connection_process(data, addr)

        number_of_recv_heartbeat =  get_number_of_recv_heartbeat(recv_client_id)

        child.send_signal(signal.SIGINT)
        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(number_of_recv_heartbeat, TEST_NUMBER_OF_RECV_HEARTBEAT)
        self.assertEqual(return_code, expected_return_code)


    def test_recv_client_release_request(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID
        expected_return_code = 1

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_RECV_SIGINT_INPUT)

        data, addr = server_socket.receive()
        recv_client_id = connection_process(data, addr)

        child.send_signal(signal.SIGINT)

        data, addr = server_socket.receive()
        api_id = unpack_common_message_header(data)
        if api_id == CLIENT_RELEASE_REQUEST_API_ID:
            recv_release_client_id = unpack_client_release_request_data(data)

        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(recv_release_client_id, expected_client_id)
        self.assertEqual(return_code, expected_return_code)


    def test_send_chat_msg(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID
        expected_return_code = 1

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_SEND_CHAT_MSG_INPUT)

        data, addr = server_socket.receive()
        recv_client_id = connection_process(data, addr)

        data, addr = server_socket.receive()
        api_id = unpack_common_message_header(data)
        if api_id == CLIENT_SEND_CHAT_MESSAGE_API_ID:
            source_client_id, dest_client_id, msg = unpack_client_chat_message_data(data)

        child.send_signal(signal.SIGINT)
        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(source_client_id, expected_client_id)
        self.assertEqual(dest_client_id, TEST_CHAT_DEST_CLIENT_ID)
        self.assertEqual(msg, TEST_CHAT_MSG)
        self.assertEqual(return_code, expected_return_code)


    def test_recv_gataway_release_notification(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID
        expected_return_code = 1

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_RECV_SIGINT_INPUT)

        data, addr = server_socket.receive()
        recv_client_id = connection_process(data, addr)

        send_data = pack_gataway_release_notification_data()
        server_socket.server.sendto(send_data, addr)

        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(return_code, expected_return_code)


    def test_chat_failure_response(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_RECV_SIGINT_INPUT)

        data, addr = server_socket.receive()
        recv_client_id = connection_process(data, addr)

        send_data = pack_client_common_response_data(CHAT_MESSAGE_RESPONSE_API_ID, recv_client_id, RESPONSE_STATUS_ERROR, 0)
        server_socket.server.sendto(send_data, addr)

        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(return_code, 1)


    def test_recv_chat_msg(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_RECV_SIGINT_INPUT)

        data, addr = server_socket.receive()
        recv_client_id = connection_process(data, addr)

        send_data = pack_client_chat_msg_data(CLIENT_SEND_CHAT_MESSAGE_API_ID, 124, recv_client_id, len("hello"), "hello")
        server_socket.server.sendto(send_data, addr)

        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(return_code, 1)


    def test_recv_invalid_api_id(self):
        # Given
        expected_client_id = TEST_EXPECTED_CLIENT_ID

        # When
        child = start_client(TEST_GENERAL_STARTUP_CONMAND, TEST_RECV_SIGINT_INPUT)

        data, addr = server_socket.receive()
        recv_client_id = connection_process(data, addr)

        send_data = pack_invalid_api_id_data(20)
        server_socket.server.sendto(send_data, addr)

        return_code = stop_cient(child)

        # Then
        self.assertEqual(recv_client_id, expected_client_id)
        self.assertEqual(return_code, 1)


if __name__ == '__main__':
    unittest.main(verbosity=2)
