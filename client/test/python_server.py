import socket
import os
import threading
from message_service import *
from constanst import *


receive_handle = {
    CLIENT_CONNECTION_REQUEST_API_ID: receive_client_connect_request_handle,
    GATEWAY_CONNECTION_REQUEST_API_ID: receive_gataway_connect_request_handle,
    CLIENT_SEND_CHAT_MESSAGE_API_ID: receive_chat_message_handle,
}


def receive_data():
    while True:
        data, addr = server_socket.server.recvfrom(BUFSIZE)
        api_id = unpack_common_message_header(data)
        print("api_id = ", api_id)
        if api_id in receive_handle:
            handle_func = receive_handle[api_id]
            handle_func(data, addr)

        elif api_id == CLIENT_HEARTBEAT_API_ID:
            client_id = unpack_client_heartbeat_data(data)
            print("client_id = ", client_id)
            print("Receive a client heartber from ", addr)

        elif api_id == CLIENT_RELEASE_REQUEST_API_ID:
            client_id = unpack_client_release_request_data(data)
            print("client_id = ", client_id)
            print("Receive a client release request from ", addr)


if __name__ == '__main__':
    server_socket.start_udp(SERVER_IP, SERVER_PORT)
    r_t = threading.Thread(target=receive_data)
    r_t.start()
    r_t.join()
    while(1):
        pass
