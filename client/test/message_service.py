import struct
import socket
from constanst import *

BUFSIZE = 1024
user_list = {}

class UDPSocket():

    def start_udp(self, ip, port):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((ip, port))
        # print('starting udp server on %s' % ip, port)

    def close_udp(self):
        self.server.close()
        self.server = None


    def receive(self):
        data, addr = self.server.recvfrom(BUFSIZE)
        # print "Recv", data
        return data, addr


server_socket = UDPSocket()

def pack_client_release_response_data(api_id, client_id, status, error_code):
    data_to_send = struct.pack("!BHHBB", api_id, client_id, status, error_code)
    return data_to_send


def pack_client_connect_response_data(api_id, client_id, status, error_code):
    data_to_send = struct.pack("!BHBB", api_id, client_id, status, error_code)
    return data_to_send


def pack_client_common_response_data(api_id, client_id, status, error_code):
    data_to_send = struct.pack("!BHBB", api_id, client_id, status, error_code)
    return data_to_send


def pack_client_chat_msg_data(api_id, source_client_id, destination_client_id, chat_msg_len, chat_msg):
    data_to_send = struct.pack("!BHHH%ds" % chat_msg_len, api_id, source_client_id, destination_client_id,
    chat_msg_len, bytes(chat_msg.encode('utf-8')))
    return data_to_send


def pack_gataway_release_notification_data():
    data_to_send = struct.pack("!B", GATEWAY_RELEASE_NOTICE_API_ID)
    return data_to_send


def pack_invalid_api_id_data(invalid_api_id):
    data_to_send = struct.pack("!B", invalid_api_id)
    return data_to_send


def unpack_common_message_header(data):
    api_ud = struct.unpack("!B", data[:1])[0]
    return api_ud


def unpack_client_connect_request_data(data):
    client_id = struct.unpack("!BH", data[:3])[1]
    return client_id


def unpack_client_heartbeat_data(data):
    client_id = struct.unpack("!BH", data[:3])[1]
    return client_id


def unpack_client_release_request_data(data):
    client_id = struct.unpack("!BH", data[:3])[1]
    return client_id


def unpack_client_chat_message_data(data):
    unpack_data = struct.unpack("!BHHH", data[:7])
    msg_len = unpack_data[3]
    msg = struct.unpack("!%ds" % msg_len, data[7:])[0].decode().strip('\0')
    source_client_id = unpack_data[1]
    dest_client_id = unpack_data[2]
    return source_client_id, dest_client_id, msg


def receive_client_connect_request_handle(recv_data, addr):
    client_id = unpack_client_connect_request_data(recv_data)
    print(client_id)
    if client_id not in user_list:
        user_list[client_id] = addr
        send_data = pack_client_connect_response_data(CLIENT_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_OK, 0)
    else:
        send_data = pack_client_connect_response_data(CLIENT_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_ERROR, CONNECTED_ERR_OF_ID_EXISTED)
    server_socket.server.sendto(send_data, addr)


def receive_gataway_connect_request_handle(recv_data, addr):
    client_id = unpack_client_connect_request_data(recv_data)
    if client_id not in user_list:
        user_list[client_id] = addr
        send_data = pack_client_connect_response_data(GATEWAY_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_OK, 0)
    else:
        send_data = pack_client_connect_response_data(GATEWAY_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_ERROR, CONNECTED_ERR_OF_ID_EXISTED)
    server_socket.server.sendto(send_data, addr)


def receive_client_heartbeat_handle(recv_data, addr):
    client_id = unpack_client_connect_request_data(recv_data)
    send_data = pack_client_connect_response_data(GATEWAY_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_OK, 0)
    server_socket.server.sendto(send_data, addr)


def receive_chat_message_handle(recv_data, addr):
    print("Receive a chat message from ", addr)
    source_client_id, dest_client_id, msg = unpack_client_chat_message_data(recv_data)

    print("dest_client_id = ", dest_client_id)

    if dest_client_id in user_list:
        dest_addr = user_list[dest_client_id]
        print("Transfer chat message to ", dest_addr)
        server_socket.server.sendto(recv_data, dest_addr)
    else:
        send_data = pack_client_common_response_data(CHAT_MESSAGE_RESPONSE_API_ID, source_client_id, RESPONSE_STATUS_ERROR, TARGET_CLIENT_OFFLINE_ERR)
        server_socket.server.sendto(send_data, addr)