import subprocess
from message_service import *
from constanst import *


def start_client(command, input):
    child = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL, encoding="utf-8")

    child.stdin.write(input)
    child.stdin.flush()
    child.stdin.close()
    return child


def stop_cient(child):
    child.wait()
    return child.returncode


def connection_process(data, addr):
    client_id = unpack_client_connect_request_data(data)
    if client_id == TEST_EXPECTED_CLIENT_ID:
        send_data = pack_client_connect_response_data(CLIENT_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_OK, 0)
    else:
        send_data = pack_client_connect_response_data(CLIENT_CONNECTION_RESPONSE_API_ID, client_id, RESPONSE_STATUS_ERROR, 0)
    server_socket.server.sendto(send_data, addr)
    return client_id


def get_number_of_recv_heartbeat(recv_client_id):
    number_of_recv_heartbeat = 0

    while number_of_recv_heartbeat < TEST_NUMBER_OF_RECV_HEARTBEAT:
        data, addr = server_socket.receive()
        api_id = unpack_common_message_header(data)
        if api_id == CLIENT_HEARTBEAT_API_ID :
            recv_heartbeat_client_id = unpack_client_heartbeat_data(data)
            if recv_heartbeat_client_id == recv_client_id:
                number_of_recv_heartbeat += 1

    return number_of_recv_heartbeat
