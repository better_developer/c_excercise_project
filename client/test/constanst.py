CLIENT_CONNECTION_REQUEST_API_ID = 0
CLIENT_CONNECTION_RESPONSE_API_ID = 1
CONNECTION_REQUEST_FORWARDING_API_ID = 2
GATEWAY_CONNECTION_REQUEST_API_ID = 3
GATEWAY_CONNECTION_RESPONSE_API_ID = 4
CLIENT_RELEASE_REQUEST_API_ID = 5
GATEWAY_RELEASE_NOTICE_API_ID = 6
GATEWAT_RELEASE_REQUEST_API_ID = 7
CLIENT_HEARTBEAT_API_ID = 8
GATEWAY_HEARTBEAT_API_ID = 9
CLIENT_RELEASE_NOTICE_FROM_GATEWAY_API_ID = 10
CLIENT_SEND_CHAT_MESSAGE_API_ID = 11
CHAT_MESSAGE_RESPONSE_API_ID = 12

RESPONSE_STATUS_OK = 0
RESPONSE_STATUS_ERROR = 1

CONNECTED_ERR_OF_ID_EXISTED = 100
TARGET_CLIENT_OFFLINE_ERR = 250

SERVER_IP = "127.0.0.1"
SERVER_PORT = 8888

BUFSIZE = 1024

TEST_EXPECTED_CLIENT_ID = 123
TEST_UNEXPECTED_CLIENT_ID = 124
TEST_NUMBER_OF_RECV_HEARTBEAT = 2
TEST_CHAT_MSG = "hello\n"
TEST_CHAT_DEST_CLIENT_ID = 124

TEST_EXPECTED_RETURN_CODE = 1
TEST_GENERAL_STARTUP_CONMAND = "../bin/main"
TEST_INVALID_STARTUP_CONMAND = ["../bin/main", "1", "1"]
TEST_VALID_ALL_IPV4_ADDR_STARTUP_CONMAND = ["../bin/main", "0.0.0.0"]
TEST_VALID_CLIENT_LOCAL_IP_STARTUP_CONMAND = ["../bin/main", "127.0.0.1"]
TEST_INVALID_CLIENT_IP_STARTUP_CONMAND = ["../bin/main", "192.0.1.1"]

TEST_CORRECT_CONNECTION_INPUT = "127.0.0.1\n"+ str(TEST_EXPECTED_CLIENT_ID) + "\n0\n"
TEST_UNEXPECTED_CONNECTION_INPUT = "127.0.0.1\n"+ str(TEST_UNEXPECTED_CLIENT_ID) + "\n"
TEST_CONNECT_INVALID_REMOTE_IP_INPUT = "127\n"
TEST_RECV_SIGINT_INPUT = "127.0.0.1\n"+ str(TEST_EXPECTED_CLIENT_ID) + "\n"
TEST_RECV_CLIENT_HEARTBEAT_INPUT = "127.0.0.1\n"+ str(TEST_EXPECTED_CLIENT_ID) + "\n1\n"
TEST_SEND_CHAT_MSG_INPUT = "127.0.0.1\n"+ str(TEST_EXPECTED_CLIENT_ID) + "\n" + str(TEST_CHAT_DEST_CLIENT_ID) + "\n" + TEST_CHAT_MSG + "\n"

TEST_IP_ADDR = "127.0.0.1"
