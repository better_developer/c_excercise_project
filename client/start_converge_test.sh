make clean
make
cd test/
python3 client_test.py
cd -
cd obj/
lcov -c -d . -o result.info
genhtml -o ../result result.info
