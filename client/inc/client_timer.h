#pragma once

void ClientTimer_Init(void);
void ClientTimer_StartConnectionTimer(void);
void ClientTimer_StartHeartbeatTimer(void);
void ClientTimer_StopConnectionTimer(void);
void ClientTimer_StopHeartbeatTimer(void);
void ClientTimer_Deinit(void);
