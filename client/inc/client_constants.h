#pragma once

#define kREMOTE_PORT 8888
#define HEARTBEAT_TIMER_DEFAULT_TIME 10
#define HEARTBEAT_DEFAULT_TIME 10
#define CONNECTION_DEFAULT_TIME 10

#define kCLIENT_CONNECTED_STATUS 0
#define kCLIENT_DISCONNECTED_STATUS 1
#define kRECEIVE_BUFFER_SIZE 1024
#define kCHAT_MSG_SIZE 1016

#define kCONNECTION_FAILURE_INFO "Connection failure\n"
#define kCONNECTION_SUCCESSFUL_INFO "Connection successful\n"
#define kSEND_CHAT_MSG_FAILURE_INFO "Failed to send chat message!\n"
#define kRECEIVE_GATEWAY_RELEASE_NOTIFICATION_INFO "Receive gateway release notification.\n"
#define kRECEIVE_INVALID_API_ID "Receive invalid api id.\n"

#define kCLIENT_SOCKET_BIND_FAILURE -1
#define kCLIENT_SOCKET_BIND_SUCCESS 0
#define kCLIENT_SOCKET_INVALID_IP 0
#define IPADDR_MAX_SIZE 20
