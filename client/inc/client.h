#pragma once

#include "type.h"

void Client_Init(char *);
void Client_Deinit(void);
UInt16 Client_GetId(void);
void Client_SetConnectionStatus(UInt16);
void Client_StartConnection(void);
void Client_StartConversation(void);
