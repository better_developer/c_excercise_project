#pragma once

#include "message_format.h"

void Message_SendConnectRequset(void);
void Message_SendReleaseRequset(void);
void Message_SendHeartbeat(void);
void Message_SendChatMessage(unsigned short, const void *);
void Message_UnpackClientConnectResponseData(const void *const recvbuf, struct common_reponse *data);
void Message_UnpackClientChatMessageResponseData(const void *const recvbuf, struct common_reponse *data);
void Message_UnpackGatewayNoticeClientGatewayReleaseData(const void *const recvbuf, struct client_release_notice_from_gateway *data);
