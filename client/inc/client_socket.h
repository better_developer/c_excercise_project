#pragma once

#include <arpa/inet.h>
#include "type.h"

void ClientSocket_Init(char *client_ip);
void ClientSocket_Connect(const char * const ip, const UInt16 port);
void ClientSocket_Bind(const char * const ip, UInt16 port);
int ClientSocket_Send(void * const sendbuf, const size_t buf_size);
int ClientSocket_Receive(const size_t buf_size, void *recv_buf, struct sockaddr_in *recv_addr);
void ClientSocket_Deinit(void);
